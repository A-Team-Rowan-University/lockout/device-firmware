/*
 * interface.cpp
 *
 * Author: Andrew Hollabaugh
 * Date Created: 6/27/20
 *
 * Interface between target-specific hardware and application code
 * Fits LockoutGateway hw_conf struct with all the hardware LockoutGateway needs access to
 * Includes interrupt callbacks, which just call identical functions in LockoutGateway
 */

#include "interface.h"
#include "LockoutGateway.h"
#include "SPIRIT_Gpio.h"

static LockoutGateway *p_lockout_gateway;

LockoutGateway::GatewayHWConf gateway_hw_conf {
		.radio_hw_conf {
				.p_hspi = &hspi1,
				.NSS_GPIO_PORT = RADIO_CSN_GPIO_Port,
				.NSS_GPIO_PIN = RADIO_CSN_Pin,
				.SDN_GPIO_PORT = RADIO_SDN_GPIO_Port,
				.SDN_GPIO_PIN = RADIO_SDN_Pin,
				.INT_GPIO_PORT = RADIO_INT_GPIO_Port,
				.INT_GPIO_PIN = RADIO_INT_Pin,
				.INT_IRQ_HANDLER = RADIO_INT_EXTI_IRQn,
				.SPIRIT_INT_GPIO_PIN = SPIRIT_GPIO_3,
		},

		//.p_debug_comms_huart = &huart1,
		.p_rpi_comms_huart = &huart3,

		.LED_HB_GPIO_PORT = LED0_GPIO_Port,
		.LED_HB_GPIO_PIN = LED0_Pin,

		.LED_RX_GPIO_PORT = LED1_GPIO_Port,
		.LED_RX_GPIO_PIN = LED1_Pin,
		.p_rx_led_htim = &htim16,

		.LED_TX_GPIO_PORT = LED2_GPIO_Port,
		.LED_TX_GPIO_PIN = LED2_Pin,
		.p_tx_led_htim = &htim17,
};

/*
 * Constructs a LockoutGateway and runs its main loop
 * Called from main.c
 */
void lockout_gateway_main()
{
	p_lockout_gateway = new LockoutGateway(&gateway_hw_conf);
    p_lockout_gateway->run();
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef* htim)
{
	if(htim->Instance == TIM16)
	{
		p_lockout_gateway->on_rx_led_timer_overflow();
	}
	else if(htim->Instance == TIM17)
	{
		p_lockout_gateway->on_tx_led_timer_overflow();
	}
}

void HAL_GPIO_EXTI_Falling_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin==gateway_hw_conf.radio_hw_conf.INT_GPIO_PIN)
	{
		p_lockout_gateway->on_radio_int();
	}
}

void on_rpi_uart_rx(uint8_t *buf)
{
	if(p_lockout_gateway != nullptr)
	{
		p_lockout_gateway->on_rpi_uart_rx(buf);
	}
}
