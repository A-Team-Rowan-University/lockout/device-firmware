/*
 * EEPROM.h
 *
 * Author: Andrew Hollabaugh
 * Date Created: 3/07/21
 *
 * Driver for AT24CS02 (probably any AT24 series IC would work) EEPROM IC
 */

#ifndef EEPROM_H
#define EEPROM_H

#include "i2c.h"
#include "main.h"

class EEPROM {
public:
	struct EEPROMHwConf {
		I2C_HandleTypeDef* p_hi2c;
		GPIO_TypeDef* WP_GPIO_PORT;
		uint16_t WP_GPIO_PIN;
	};

	explicit EEPROM(EEPROMHwConf* pmtr_p_hw_conf);

	void write_byte(uint8_t addr, uint8_t data);
	void write_bytes(uint8_t addr, uint8_t* p_data, size_t len);
	uint8_t read_byte(uint8_t addr);
	uint8_t* read_bytes(uint8_t addr, size_t len);

private:
	EEPROMHwConf* m_p_hw_conf;
	const uint8_t I2C_DEVICE_ADDR = 0b1010000;
	const uint8_t I2C_TIMEOUT = 100;

	void i2c_tx(uint8_t* p_data, size_t len);
	void i2c_rx(uint8_t* p_data, size_t len);
};

#endif //EEPROM_H
