/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g0xx_hal.h"
#include "stm32g0xx_ll_system.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define UART_RPI_RX_Pin GPIO_PIN_9
#define UART_RPI_RX_GPIO_Port GPIOB
#define EXTRA_PC14_Pin GPIO_PIN_14
#define EXTRA_PC14_GPIO_Port GPIOC
#define EXTRA_PC15_Pin GPIO_PIN_15
#define EXTRA_PC15_GPIO_Port GPIOC
#define EXTRA_PA0_Pin GPIO_PIN_0
#define EXTRA_PA0_GPIO_Port GPIOA
#define EXTRA_PA1_Pin GPIO_PIN_1
#define EXTRA_PA1_GPIO_Port GPIOA
#define EXTRA_PA2_Pin GPIO_PIN_2
#define EXTRA_PA2_GPIO_Port GPIOA
#define EXTRA_PA3_Pin GPIO_PIN_3
#define EXTRA_PA3_GPIO_Port GPIOA
#define RADIO_CSN_Pin GPIO_PIN_4
#define RADIO_CSN_GPIO_Port GPIOA
#define RADIO_SCK_Pin GPIO_PIN_5
#define RADIO_SCK_GPIO_Port GPIOA
#define RADIO_MISO_Pin GPIO_PIN_6
#define RADIO_MISO_GPIO_Port GPIOA
#define RADIO_MOSI_Pin GPIO_PIN_7
#define RADIO_MOSI_GPIO_Port GPIOA
#define RADIO_INT_Pin GPIO_PIN_0
#define RADIO_INT_GPIO_Port GPIOB
#define RADIO_INT_EXTI_IRQn EXTI0_1_IRQn
#define RADIO_SDN_Pin GPIO_PIN_1
#define RADIO_SDN_GPIO_Port GPIOB
#define LED0_Pin GPIO_PIN_2
#define LED0_GPIO_Port GPIOB
#define LED1_Pin GPIO_PIN_8
#define LED1_GPIO_Port GPIOA
#define LED2_Pin GPIO_PIN_6
#define LED2_GPIO_Port GPIOC
#define UART_DB_TX_Pin GPIO_PIN_9
#define UART_DB_TX_GPIO_Port GPIOA
#define UART_DB_RX_Pin GPIO_PIN_10
#define UART_DB_RX_GPIO_Port GPIOA
#define SWDIO_Pin GPIO_PIN_13
#define SWDIO_GPIO_Port GPIOA
#define SWCLK_Pin GPIO_PIN_14
#define SWCLK_GPIO_Port GPIOA
#define EXTRA_PA15_Pin GPIO_PIN_15
#define EXTRA_PA15_GPIO_Port GPIOA
#define EXTRA_PB3_Pin GPIO_PIN_3
#define EXTRA_PB3_GPIO_Port GPIOB
#define BUTTON_Pin GPIO_PIN_4
#define BUTTON_GPIO_Port GPIOB
#define BUTTON_EXTI_IRQn EXTI4_15_IRQn
#define EXTRA_PB5_Pin GPIO_PIN_5
#define EXTRA_PB5_GPIO_Port GPIOB
#define EXTRA_PB6_Pin GPIO_PIN_6
#define EXTRA_PB6_GPIO_Port GPIOB
#define EXTRA_PB7_Pin GPIO_PIN_7
#define EXTRA_PB7_GPIO_Port GPIOB
#define UART_RPI_TX_Pin GPIO_PIN_8
#define UART_RPI_TX_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
