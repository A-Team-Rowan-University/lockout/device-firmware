/*
 * Display.h
 *
 * Author: Andrew Hollabaugh
 * Date Created: 7/1/20
 *
 * Driver for LCD Character display using the ST7036 driver IC and I2C interface
 * Use with 'NHD-C0220BiZ-FSRGB-FBW-3V3' display
 */

#ifndef DISPLAY_H
#define DISPLAY_H

#include <cstdlib>
#include <cstring>
#include <string>
#include "tim.h"
#include "main.h"

class Display {
public:
	struct DisplayHWConf {
		TIM_HandleTypeDef* p_bklt_htim;
		uint8_t BKLT_RED_PWM_CHANNEL;
		uint8_t BKLT_GRN_PWM_CHANNEL;
		uint8_t BKLT_BLU_PWM_CHANNEL;
		I2C_HandleTypeDef* p_hi2c;
		GPIO_TypeDef* RST_GPIO_PORT;
		uint16_t RST_GPIO_PIN;
	};

	typedef enum {
		LT_TOP,
		LT_BOTTOM,
		LT_DOUBLE_HEIGHT
	} line_type_t;

    explicit Display(DisplayHWConf* pmtr_p_hw_conf);

    void init();
    void set_backlight(uint8_t red_percent, uint8_t grn_percent, uint8_t blu_percent);
	void set_contrast(uint8_t contrast_val);
	void write(uint8_t* p_line, size_t len, line_type_t line_type);
	void write_str(std::string str, line_type_t line_type);
	void write_str_double(std::string str_top, std::string str_bot);
    void clear();
private:
	DisplayHWConf* m_p_hw_conf;

    const uint32_t I2C_TIMEOUT = 100;
    const uint8_t I2C_DEVICE_ADDR = 0x78;
    const uint8_t RST_DELAY = 50;
	const uint16_t WRITE_DELAY = 2;
	const uint8_t BOTTOM_LINE_BEGIN_ADDR = 0x40;
    const uint8_t CONTRAST_MAX_VAL = 0x3f;
    const uint8_t CONTRAST_DEFAULT_VAL = 0x1f;
    const uint8_t RAB_VAL = 0x05;

    enum command : uint8_t {
        CLEAR_DISPLAY = 0x01u,
        RETURN_HOME = 0x02u,
        SHIFT_CURSOR_LEFT = 0x10u, //requires IS=0
        SHIFT_CURSOR_RIGHT = 0x14u, //requires IS=0
        SHIFT_DISPLAY_LEFT = 0x18u, //requires IS=0
        SHIFT_DISPLAY_RIGHT = 0x1Cu, //requires IS=0
        SET_DDRAM_ADDR = 0x80u, //OR with DDRAM address
        SET_ICON_ADDR = 0x40u, //OR with ICON address, requires IS=1
        CONTRAST_SET_LOW = 0x70u //OR with least significant 3 bits of contrast, requires IS=1
    };

    enum entry_mode : uint8_t {
        ENTRY_MODE_SHIFT_CURSOR_LEFT = 0x04u,
        ENTRY_MODE_SHIFT_CURSOR_RIGHT = 0x06u,
        ENTRY_MODE_SHIFT_DISPLAY_LEFT = 0x07u,
        ENTRY_MODE_SHIFT_DISPLAY_RIGHT = 0x05u
    };

    enum display_mode : uint8_t {
    	DISPLAY_MODE_MASK = 0x08u, //OR
        DISPLAY_MODE_DISPLAY_OFF = 0x00u, //OR
        DISPLAY_MODE_DISPLAY_ON = 0x04u,
        DISPLAY_MODE_CURSOR_OFF = 0x00u, //OR
        DISPLAY_MODE_CURSOR_ON = 0x02u,
        DISPLAY_MODE_BLINK_OFF = 0x00u, //OR
        DISPLAY_MODE_BLINK_ON = 0x01u
    };

    enum function_set : uint8_t {
        FUNCTION_SET_DATA_LENGTH_4BIT = 0x20u, //OR 1/3
        FUNCTION_SET_DATA_LENGTH_8BIT = 0x30u,
        FUNCTION_SET_SINGLE_LINE = 0x20u, //OR 2/3
        FUNCTION_SET_SINGLE_LINE_DOUBLE_HEIGHT = 0x24u,
        FUNCTION_SET_DOUBLE_LINE = 0x28u,
        FUNCTION_SET_NORMAL_INSTRUCTION = 0x00u, //OR 3/3
        FUNCTION_SET_EXTENDED_INSTRUCTION = 0x01u
    };

	//requires IS = 1 (extended instruction table)
	enum bias_osc : uint8_t {
		BIAS_OSC_BIAS_1_5 = 0x10, //OR 1/2
		BIAS_OSC_BIAS_1_4 = 0x18,
		BIAS_OSC_FREQ_183 = 0x14 //OR 2/2
	};

    //requires IS = 1 (extended instruction table)
    enum icon_control : uint8_t { //least significant 2 bits are C5, C4
        ICON_CONTROL_ICON_OFF = 0x50u, //OR 1/3
        ICON_CONTROL_ICON_ON = 0x51u,
        ICON_CONTROL_BOOST_OFF = 0x50u, //OR 2/3
        ICON_CONTROL_BOOST_ON = 0x54u,
    };

    //requires IS = 1 (extended instruction table)
    enum follower_control : uint8_t { //least significant 3 bits are Rab
        FOLLOWER_CONTROL_FOLLOWER_OFF = 0x60u, //OR 1/2
        FOLLOWER_CONTROL_FOLLOWER_ON = 0x68u,
    };

	void set_backlight_channel(uint8_t pwm_channel, uint8_t brightness_percent);
    void write_instruction(uint8_t inst);
	void write_data(uint8_t* p_data, size_t length);
    void i2c_transmit(uint8_t* p_data, size_t len);
};

#endif //DISPLAY_H
