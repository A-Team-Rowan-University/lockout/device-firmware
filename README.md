# Device Firmware
Firmware for the STM32 processors in the devices and gateways for the lockout project.

The lockout project provides a network framework for many possible device types, not just lockout devices. Gateways 
will be common for the entire system, even if completely unrelated devices are connected to the same gateway.

## Directories
- bin/ - compiled binaries
- common-inc/ - header files common to multiple platforms
- common-src/ - source files common to multiple platforms
- lockout-gateway/ - headers, sources, drivers, other stuff specific to STM32G071 on the gateways
- lockout-nucleol053-devboard - headers, sources, drivers, other stuff specific to the Nucleo Development Board 
  with STM32L053, configured as a device
