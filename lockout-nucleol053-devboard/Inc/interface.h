/*
 * interface.h
 *
 * Author: Andrew Hollabaugh
 * Date Created: 6/27/20
 *
 * Interface layer between main.c (C) and LockoutDevice.cpp (C++)
 */

#ifndef INTERFACE_H
#define INTERFACE_H


#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "tim.h"

void lockout_device_main();

#ifdef __cplusplus
}
#endif

#endif //INTERFACE_H
