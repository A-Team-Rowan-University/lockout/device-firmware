/*
 * interface.cpp
 *
 * Author: Andrew Hollabaugh
 * Date Created: 5/17/21
 *
 * Interface between target-specific hardware and application code
 * Fits LockoutDevice hw_conf struct with all the hardware LockoutDevice needs access to
 * Includes interrupt callbacks, which just call identical functions in LockoutDevice
 */

#include "interface.h"
#include "LockoutDevice.h"
#include "SPIRIT_Gpio.h"
#include "i2c.h"

static LockoutDevice *p_lockout_device;

LockoutDevice::DeviceHWConf device_hw_conf {
		.radio_hw_conf {
				.p_hspi = &hspi1,
				.NSS_GPIO_PORT = RADIO_CSN_GPIO_Port,
				.NSS_GPIO_PIN = RADIO_CSN_Pin,
				.SDN_GPIO_PORT = RADIO_SDN_GPIO_Port,
				.SDN_GPIO_PIN = RADIO_SDN_Pin,
				.INT_GPIO_PORT = RADIO_INT_GPIO_Port,
				.INT_GPIO_PIN = RADIO_INT_Pin,
				.INT_IRQ_HANDLER = RADIO_INT_EXTI_IRQn,
				.SPIRIT_INT_GPIO_PIN = SPIRIT_GPIO_3,
		},

		.card_reader_hw_conf {
				.p_hspi = &hspi2,
				.NSS_GPIO_PORT = PCD_NSS_GPIO_Port,
				.NSS_GPIO_PIN = PCD_NSS_Pin,
				.RST_GPIO_PORT = PCD_RST_GPIO_Port,
				.RST_GPIO_PIN = PCD_RST_Pin
		},

		.display_hw_conf {
			.p_bklt_htim = &htim1,
			.BKLT_RED_PWM_CHANNEL = TIM_CHANNEL_1,
			.BKLT_GRN_PWM_CHANNEL = TIM_CHANNEL_2,
			.BKLT_BLU_PWM_CHANNEL = TIM_CHANNEL_3,
			.p_hi2c = &hi2c1,
			.RST_GPIO_PORT = LCD_RST_GPIO_Port,
			.RST_GPIO_PIN = LCD_RST_Pin
		},

		.buzzer_hw_conf {
			.p_htim_pwm = &htim16,
			.p_htim_note = &htim6,
			.PWM_CHANNEL = TIM_CHANNEL_1,
			.CPU_FREQ = 32000000,
			.PWM_TIM_PRESCALER = 32,
			.NOTE_TIM_PRESCALER = 8192
		},

		.eeprom_hw_conf {
			.p_hi2c = &hi2c2,
			.WP_GPIO_PORT = EEPROM_WP_GPIO_Port,
			.WP_GPIO_PIN = EEPROM_WP_Pin
		},

		.p_debug_comms_huart = &huart1,

		.LED_HB_GPIO_PORT = LED0_GPIO_Port,
		.LED_HB_GPIO_PIN = LED0_Pin,

		.LED_RX_GPIO_PORT = LED2_GPIO_Port,
		.LED_RX_GPIO_PIN = LED2_Pin,
		.p_rx_led_htim = &htim14,

		.LED_TX_GPIO_PORT = LED3_GPIO_Port,
		.LED_TX_GPIO_PIN = LED3_Pin,
		.p_tx_led_htim = &htim15,

		//.BTN_PING_GPIO_PORT = BTN1_GPIO_Port,
		.BTN_PING_GPIO_PIN = BTN1_Pin,

		//.BTN_EX_GPIO_PORT = BTN0_GPIO_Port,
		//.BTN_EX_GPIO_PIN = BTN0_Pin,

		.EXT_BTN0_GPIO_PORT = EXT_BTN0_GPIO_Port,
		.EXT_BTN0_GPIO_PIN = EXT_BTN0_Pin,

		.EXT_BTN1_GPIO_PORT = EXT_BTN1_GPIO_Port,
		.EXT_BTN1_GPIO_PIN = EXT_BTN1_Pin,

		.EXT_BTN2_GPIO_PORT = EXT_BTN2_GPIO_Port,
		.EXT_BTN2_GPIO_PIN = EXT_BTN2_Pin,

		.EXT_LED_GPIO_PORT = EXT_LED_GPIO_Port,
		.EXT_LED_GPIO_PIN = EXT_LED_Pin,

		.MCHN_EN_GPIO_PORT = MCHN_EN_GPIO_Port,
		.MCHN_EN_GPIO_PIN = MCHN_EN_Pin,

		.PB_FB_GPIO_PORT = PB_FB_GPIO_Port,
		.PB_FB_GPIO_PIN = PB_FB_Pin,

		.CARD_SW_GPIO_PORT = CARD_INS_SW_GPIO_Port,
		.CARD_SW_GPIO_PIN = CARD_INS_SW_Pin,

		.KEY_ENABLE_GPIO_PORT = KEY_ENABLE_GPIO_Port,
		.KEY_ENABLE_GPIO_PIN = KEY_ENABLE_Pin,

		.KEY_DISABLE_GPIO_PORT = KEY_DISABLE_GPIO_Port,
		.KEY_DISABLE_GPIO_PIN = KEY_DISABLE_Pin,
};

/*
 * Constructs a LockoutDevice and runs its main loop
 * Called from main.c
 */
void lockout_device_main()
{
	p_lockout_device = new LockoutDevice(&device_hw_conf);
    p_lockout_device->run();
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef* htim)
{
	if(htim->Instance == TIM14)
    {
	    p_lockout_device->on_rx_led_timer_overflow();
    }
	else if(htim->Instance == TIM15)
	{
		p_lockout_device->on_tx_led_timer_overflow();
	}
	else if(htim->Instance == TIM6)
	{
		p_lockout_device->on_note_timer_overflow();
	}
}
void HAL_GPIO_EXTI_Falling_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin == device_hw_conf.radio_hw_conf.INT_GPIO_PIN)
	{
		p_lockout_device->on_radio_int();
	}
	else if(GPIO_Pin == device_hw_conf.BTN_PING_GPIO_PIN)
	{
		p_lockout_device->on_ping_button_press();
	}
}
