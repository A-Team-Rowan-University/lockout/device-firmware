/*
 * Radio.cpp
 *
 * Author: Andrew Hollabaugh
 * Date Created: 6/27/20
 *
 * Application functionality for using the SPIRIT1 radio via the SPIRIT1 X-CUBE-SUBG1 library
 */

#include "Radio.h"

Radio::Radio(Comms* pmtr_p_comms)
{
	m_p_comms = pmtr_p_comms;
}

void Radio::init()
{
	RadioEnterShutdown(); //restart radio
	RadioExitShutdown();

	//read status register and wait for ready status
	do
	{
		for(volatile uint8_t i = 0; i != 0xff; i++); //delay for state transition
		SpiritRefreshStatus(); //read the MC_STATUS register
	} while(g_xStatus.MC_STATE != MC_STATE_READY);

	SpiritRadioSetXtalFrequency(XTAL_FREQUENCY);

	//disable and enable interrupts
	RadioGpioInterruptCmd(RADIO_INT_EXTI_IRQn, 0x04, 0x04, DISABLE);
	RadioGpioInterruptCmd(RADIO_INT_EXTI_IRQn, 0x04, 0x04, ENABLE);

	SpiritGpioInit(&GpioInit);
	SpiritRadioInit(&RadioInit);

	SpiritRadioSetPALeveldBm(POWER_INDEX, POWER_DBM);
	SpiritRadioSetPALevelMaxIndex(POWER_INDEX);

	SpiritPktBasicInit(&BasicPktInit);

	enable_sqi();

	SpiritQiSetRssiThresholddBm(RSSI_THRESHOLD);

	SpiritAesMode(S_ENABLE);
	SpiritAesWriteKey(aes_key);
}

/*
 * Set the device's address and update register in radio IC
 */
void Radio::set_addr(uint8_t new_addr)
{
	BasicPktAddressesInit.cMyAddress = new_addr;
	SpiritPktBasicAddressesInit(&BasicPktAddressesInit);
}

/*
 * Transmit a packet over the radio
 * addr - destination address of receiver
 * tx_payload - pointer to payload to transmit
 */
void Radio::start_tx(uint8_t addr, payload_t* tx_payload)
{
	SpiritPktBasicAddressesInit(&BasicPktAddressesInit);

	//copy payload fields into a buffer
	uint8_t buf[PAYLOAD_LEN_FULL] = {0};
	buf[0] = (uint8_t)(tx_payload->cmd);
	buf[1] = tx_payload->source_addr;
	buf[2] = tx_payload->tag;
	buf[3] = tx_payload->retry;

	for(uint8_t i = 0; i < PAYLOAD_DATA_LEN; i++)
	{
		buf[i + (PAYLOAD_LEN - PAYLOAD_DATA_LEN)] = tx_payload->data[i];
	}

	uint8_t buf_enc[PAYLOAD_LEN_FULL] = {0}; //32 byte buffer, since encryption is done in 16-byte batches

	aes_use(false, buf, buf_enc); //encrypt first 16 bytes
	aes_use(false, &buf[16], &buf_enc[16]); //encrypt last 16 bytes

	SpiritIrqDeInit(nullptr); //disable irq
	SpiritIrq(TX_DATA_SENT, S_ENABLE); //enable tx irq
	SpiritPktBasicSetPayloadLength(PAYLOAD_LEN_FULL);
	SpiritIrqClearStatus(); //bank irq registers
	SpiritPktBasicSetDestinationAddress(addr);

	if(g_xStatus.MC_STATE==MC_STATE_RX)
	{
		SpiritCmdStrobeSabort(); //put radio in the READY state if in RX state
	}

	//enable CSMA
	SpiritRadioPersistenRx(S_DISABLE);
	SpiritRadioCsBlanking(S_DISABLE);
	SpiritCsmaInit(&CSMAInit);
	SpiritCsma(S_ENABLE);
	SpiritQiSetRssiThresholddBm(CSMA_RSSI_THRESHOLD);

	//fit the TX FIFO
	SpiritCmdStrobeFlushTxFifo();
	SpiritSpiWriteLinearFifo(PAYLOAD_LEN_FULL, buf_enc);

	//send the TX command
	SpiritCmdStrobeTx();

	uint8_t rssi_dummy = 0;
	print_payload(&m_p_comms->TX_START_CHAR, tx_payload, &addr, &rssi_dummy);
}

/*
 * Start listening for packets to receive
 * timeout - abort listening when timeout expires, or set to 0 to disable
 */
void Radio::start_rx(float timeout)
{
	SpiritPktBasicAddressesInit(&BasicPktAddressesInit);

	SpiritIrqDeInit(nullptr); //disable irq

	//enable rx irq
	SpiritIrq(RX_DATA_READY, S_ENABLE);
	SpiritIrq(RX_DATA_DISC, S_ENABLE);
	SpiritIrq(RX_TIMEOUT, S_ENABLE);

	SpiritPktBasicSetPayloadLength(PAYLOAD_LEN_FULL);

	//set rx timeout
	if(timeout == 0)
	{
		SET_INFINITE_RX_TIMEOUT();
		SpiritTimerSetRxTimeoutStopCondition(ANY_ABOVE_THRESHOLD);
	}
	else
	{
		SpiritTimerSetRxTimeoutMs(timeout);
		enable_sqi();
		SpiritTimerSetRxTimeoutStopCondition(RSSI_AND_SQI_ABOVE_THRESHOLD);
	}

	SpiritIrqClearStatus(); //bank irq registers

	if(g_xStatus.MC_STATE==MC_STATE_RX)
	{
		SpiritCmdStrobeSabort();
	}

	//RX command
	SpiritCmdStrobeRx();
}

/*
 * Get the payload from the last received packet
 * rx_payload - pointer to payload_t object, its fields will be updated with the new data
 * returns false if the payload was the wrong length, true if correct length
 */
bool Radio::get_rx_payload(payload_t* rx_payload)
{
	uint8_t rx_buf[PAYLOAD_LEN_FULL] = {0};
	uint8_t len = SpiritLinearFifoReadNumElementsRxFifo(); //when rx data ready read the number of received bytes
	SpiritSpiReadLinearFifo(len, rx_buf); //read the RX FIFO
	SpiritCmdStrobeFlushRxFifo();

	if(len != PAYLOAD_LEN_FULL)
	{
		return false;
	}
	else
	{
		uint8_t rx_buf_dec[PAYLOAD_LEN_FULL] = {0}; //32 byte buffer, since decryption happens in 16 byte batches

		aes_use(true, rx_buf, rx_buf_dec); //decrypt first 16 bytes
		aes_use(true, &rx_buf[16], &rx_buf_dec[16]); //decrypt last 16 bytes

		//copy data from decrypted buffer to fields of rx_payload
		rx_payload->cmd = (cmd_t)(rx_buf_dec[0]);
		rx_payload->source_addr = rx_buf_dec[1];
		rx_payload->tag = rx_buf_dec[2];
		rx_payload->retry = rx_buf_dec[3];
		for(uint8_t i = 0; i < PAYLOAD_DATA_LEN; i++)
		{
			rx_payload->data[i] = rx_buf_dec[i + (PAYLOAD_LEN - PAYLOAD_DATA_LEN)];
		}

		uint8_t rssi = SpiritQiGetRssi();
		print_payload(&m_p_comms->RX_START_CHAR, rx_payload, &BasicPktAddressesInit.cMyAddress, &rssi);
		return true;
	}
}

/*
 * Use the AES encryption coprocessor in the SPIRIT1 IC, for either encryption or decryption
 * enc_dec - false to encrypt data, true to decrypt data
 * start_buf - pointer to data to encrypt/decrypt
 * end_buf - where to write newly encrypted/decrypted data
 */
void Radio::aes_use(bool enc_dec, uint8_t* start_buf, uint8_t* end_buf)
{
	SpiritAesWriteDataIn(start_buf, 16); //send the data to the encryption coprocessor
	SpiritIrq(AES_END, S_ENABLE);
	enc_dec ? SpiritAesDeriveDecKeyExecuteDec() : SpiritAesExecuteEncryption(); //send the encryption or decryption command based on enc_dec
	while(!aes_end); //loop until aes_end flag is set by interrupt
	aes_end = false;
	SpiritIrq(AES_END, S_DISABLE);
	SpiritAesReadDataOut(end_buf, 16); //get the data out of the coprocessor
}

void Radio::enable_sqi()
{
	SpiritQiSetSqiThreshold(SQI_TH_0);
	SpiritQiSqiCheck(S_ENABLE);
}

/*
 * Print a payload to the serial port in a common format, used for gateway pi communiation
 * p_start_char - start character to begin serial packet, see Comms.h
 * p_payload - payload to print
 * p_dest_addr - destination address
 * p_rssi - rssi value, leave blank for tx
 */
void Radio::print_payload(uint8_t* p_start_char, Radio::payload_t* p_payload,
						  uint8_t* p_dest_addr, uint8_t* p_rssi)
{
	m_p_comms->write(p_start_char, 1);
	m_p_comms->write((uint8_t*)(&p_payload->cmd), 1);
	m_p_comms->write(&(p_payload->source_addr), 1);
	m_p_comms->write(p_dest_addr, 1);
	m_p_comms->write(&(p_payload->tag), 1);
	m_p_comms->write(&(p_payload->retry), 1);
	m_p_comms->write(p_rssi, 1);
	m_p_comms->write(p_payload->data, Radio::PAYLOAD_DATA_LEN);
	m_p_comms->write(&m_p_comms->END_CHAR, 1);
}

/*
 * Called when radio interrupt is triggered
 * Sets flags based on irq_status, which contains the type of interrupt triggered, which is updated by the library
 */
void Radio::on_radio_int()
{
	SpiritIrqGetStatus(&irq_status);

	if((irq_status.IRQ_TX_DATA_SENT) || (irq_status.IRQ_MAX_BO_CCA_REACH))
	{
		SpiritCsma(S_DISABLE);
		SpiritRadioPersistenRx(S_ENABLE);
		SpiritRadioCsBlanking(S_ENABLE);

		if(irq_status.IRQ_MAX_BO_CCA_REACH)
		{
			SpiritCmdStrobeSabort();
		}
		SpiritQiSetRssiThresholddBm(RSSI_THRESHOLD);

		tx_done = true;
	}

	if((irq_status.IRQ_RX_DATA_READY))
	{
		rx_done = true;
	}

	if (irq_status.IRQ_RX_TIMEOUT)
	{
		rx_timeout = true;
		//SpiritCmdStrobeRx();
	}

	//if data was discoarded upon (address) filtering
	if(irq_status.IRQ_RX_DATA_DISC)
	{
		//RX command - to ensure the device will be ready for the next reception
		SpiritCmdStrobeRx();
	}

	if(irq_status.IRQ_AES_END)
	{
		aes_end = true;
	}
}
