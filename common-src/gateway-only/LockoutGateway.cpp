/*
 * LockoutGateway.cpp
 *
 * Author: Andrew Hollabaugh
 * Date Created: 8/21/20
 *
 * Application code for lockout gateways - contains main loop
 */

#include "LockoutGateway.h"

LockoutGateway::LockoutGateway(GatewayHWConf* pmtr_p_gateway_hw_conf):
	m_radio_low_level(&pmtr_p_gateway_hw_conf->radio_hw_conf),
	m_radio(&m_rpi_comms),
	//m_db_comms(pmtr_p_gateway_hw_conf->p_debug_comms_huart),
	m_rpi_comms(pmtr_p_gateway_hw_conf->p_rpi_comms_huart)
{
	m_p_gateway_hw_conf = pmtr_p_gateway_hw_conf;
}

[[noreturn]] void LockoutGateway::run()
{
	//start LED timers
	HAL_TIM_Base_Start_IT(m_p_gateway_hw_conf->p_rx_led_htim);
	HAL_TIM_Base_Start_IT(m_p_gateway_hw_conf->p_tx_led_htim);

	m_radio.init();
	m_radio.set_addr(m_radio.GATEWAY_ADDR);
	m_radio.start_rx(0);

    while(true)
    {
	    //check if it is time for the heartbeat led to be toggled
	    if(HAL_GetTick() - m_hb_last_toggle_time > HB_HALF_PERIOD_TIME)
	    {
		    HAL_GPIO_TogglePin(m_p_gateway_hw_conf->LED_HB_GPIO_PORT, m_p_gateway_hw_conf->LED_HB_GPIO_PIN);
		    m_hb_last_toggle_time = HAL_GetTick();
	    }

    	//check to see if any bytes were received over uart from pi
	    if(!m_rpi_comms.rx_empty())
	    {
		    uint8_t rx_byte = m_rpi_comms.read_char(); //read the byte from the FIFO buffer
		    if(rx_byte == m_rpi_comms.GW_START_CHAR) //start of uart packet reception
		    {
			    m_rpi_rx = true;
			    m_rpi_rx_num_bytes = 0;
		    }
		    else if(rx_byte == m_rpi_comms.END_CHAR) //end of uart packet reception
		    {
			    m_rpi_rx = false;
			    if(m_rpi_rx_num_bytes != RPI_RX_LEN) //wrong number of bytes -> communication error
			    {
				    m_rpi_comms.write(&m_rpi_comms.COMM_ERR_CHAR, 1);
			    }
			    else
			    {
				    //copy sections of received uart packet to fields of a radio payload
				    Radio::payload_t tx_payload{};
				    tx_payload.cmd = (Radio::cmd_t)(m_rpi_rx_data[0]);
				    tx_payload.source_addr = m_rpi_rx_data[1];
				    uint8_t dest_addr = m_rpi_rx_data[2];
				    tx_payload.tag = m_rpi_rx_data[3];
				    tx_payload.retry = m_rpi_rx_data[4];
				    for(uint8_t i = 0; i < RPI_RX_DATA_LEN; i++)
				    {
					    tx_payload.data[i] = m_rpi_rx_data[i + (RPI_RX_LEN - RPI_RX_DATA_LEN)];
				    }

				    m_radio.start_tx(dest_addr, &tx_payload); //transmit the packet
				    m_radio_state = WAIT_FOR_TX;
			    }
		    }
		    else if(m_rpi_rx) //in middle of uart transmission
		    {
			    if(m_rpi_rx_num_bytes < RPI_RX_LEN)
			    {
				    m_rpi_rx_data[m_rpi_rx_num_bytes] = rx_byte; //copy newly received byte to buffer
				    m_rpi_rx_num_bytes++;
			    }
			    else //received too many bytes before finding an END_CHAR -> communication error
			    {
				    m_rpi_comms.write(&m_rpi_comms.COMM_ERR_CHAR, 1);
				    m_rpi_rx = false;
			    }
		    }
	    }

	    switch(m_radio_state)
	    {
	        case WAIT_FOR_RX: //radio in RX mode - listening for packets
				if(m_radio.rx_done) //radio is done receiving a packet
				{
					m_radio.rx_done = false;
					flash_rx_led();

					Radio::payload_t rx_payload{};
					uint8_t rssi;
					bool rx_len_valid = m_radio.get_rx_payload(&rx_payload); //get payload from radio
					rssi = SpiritQiGetRssi();
					if(!rx_len_valid)
					{
						m_rpi_comms.write(&m_rpi_comms.COMM_ERR_CHAR, 1);
					}
					else
					{
						//send payload to pi over uart
						m_radio.print_payload(&m_rpi_comms.GW_START_CHAR, &rx_payload, &m_radio.GATEWAY_ADDR, &rssi);
						m_radio.start_rx(0); //start listening again
					}
				}
				break;
		    case WAIT_FOR_TX: //radio in tx mode - waiting for itself to transmit the packet, very short time
				if(m_radio.tx_done) //radio done transmitting packet
				{
					m_radio.tx_done = false;
					flash_tx_led();
					m_radio.start_rx(0);
					m_radio_state = WAIT_FOR_RX;
				}
				break;
	    }
    }
}

/*
 * Turn on RX led and set a timer to turn it off shortly
 */
void LockoutGateway::flash_rx_led()
{
	HAL_GPIO_WritePin(m_p_gateway_hw_conf->LED_RX_GPIO_PORT,
	                  m_p_gateway_hw_conf->LED_RX_GPIO_PIN, GPIO_PIN_SET);
	reset_timer(m_p_gateway_hw_conf->p_rx_led_htim);
}

/*
 * Turn on TX led and set a timer to turn it off shortly
 */
void LockoutGateway::flash_tx_led()
{
	HAL_GPIO_WritePin(m_p_gateway_hw_conf->LED_TX_GPIO_PORT,
	                  m_p_gateway_hw_conf->LED_TX_GPIO_PIN, GPIO_PIN_SET);
	reset_timer(m_p_gateway_hw_conf->p_tx_led_htim);
}

void LockoutGateway::on_rx_led_timer_overflow()
{
	HAL_GPIO_WritePin(m_p_gateway_hw_conf->LED_RX_GPIO_PORT,
	                  m_p_gateway_hw_conf->LED_RX_GPIO_PIN, GPIO_PIN_RESET);
}

void LockoutGateway::on_tx_led_timer_overflow()
{
	HAL_GPIO_WritePin(m_p_gateway_hw_conf->LED_TX_GPIO_PORT,
	                  m_p_gateway_hw_conf->LED_TX_GPIO_PIN, GPIO_PIN_RESET);
}

void LockoutGateway::on_radio_int()
{
	m_radio.on_radio_int();
}

void LockoutGateway::on_rpi_uart_rx(uint8_t* buf)
{
	m_rpi_comms.on_rx(buf, 1);
}

