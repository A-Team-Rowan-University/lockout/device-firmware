/*
 * interface.h
 *
 * Author: Andrew Hollabaugh
 * Date Created: 6/27/20
 *
 * Interface layer between main.c (C) and LockoutGateway.cpp (C++)
 */

#ifndef INTERFACE_H
#define INTERFACE_H


#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "tim.h"

void lockout_gateway_main();
void on_rpi_uart_rx(uint8_t *buf);
void on_debug_uart_rx(uint8_t *buf);

#ifdef __cplusplus
}
#endif

#endif //INTERFACE_H
