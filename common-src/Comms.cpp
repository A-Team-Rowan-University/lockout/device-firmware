/*
 * Comms.cpp
 *
 * Author: Andrew Hollabaugh
 * Date Created: 8/23/20
 *
 * Handles serial (UART) communication
 * Contains buffer for received bytes
 */

#include "Comms.h"

Comms::Comms(UART_HandleTypeDef* pmtr_p_huart)
{
	m_p_huart = pmtr_p_huart;
	m_rx_len = 0;
}

/*
 * Transmit bytes over UART
 * Uses a timeout: keeps trying to transmit until transmit is successful or timeout expires
 * p_buf - pointer to data to write
 * len - number of bytes to write
 */
void Comms::write(uint8_t *p_buf, uint16_t len)
{
	uint32_t timeout = HAL_GetTick() + TX_TIMEOUT;
	while(HAL_UART_Transmit(m_p_huart, p_buf, len, TX_TIMEOUT) == HAL_BUSY && HAL_GetTick() < timeout);
}

/*
 * Transmit a std::string over UART
 * str - string to transmit
 */
void Comms::write_str(std::string str)
{
	uint8_t *p_buf = (uint8_t *) str.c_str();
	write(p_buf, str.length());
}

/*
 * Callend when a byte is received from UART via interrupt
 * Puts received bytes into the rx_buf (FIFO)
 */
void Comms::on_rx(uint8_t *p_buf, uint32_t len)
{
	volatile uint32_t clipped_len = len;

	if(m_rx_len + len > RX_BUF_LEN) //make sure we are not overflowing the buffer
	{
		clipped_len = RX_BUF_LEN - m_rx_len;
	}

	for(uint32_t i = 0; i < clipped_len; i++)
	{
		m_rx_buf[m_rx_len + clipped_len-1] = p_buf[i];
	}

	m_rx_len += clipped_len;
}

/*
 * Read a single byte from the rx buffer
 * Returns 0 if buffer is empty
 */
uint8_t Comms::read_char()
{
	if(m_rx_len > 0)
	{
		uint8_t c = m_rx_buf[0];
		m_rx_len -= 1;

		for(uint32_t i = 0; i < m_rx_len; i++)
		{
			m_rx_buf[i] = m_rx_buf[i + 1];
		}

		return c;
	}
	else
	{
		return 0;
	}
}

/*
 * Read a line of bytes; all the bytes in the buffer until the next line feed
 * returns a std::string
 */
std::string Comms::read_line()
{
	uint32_t line_len = 0;
	for (uint32_t i = 0; i < m_rx_len; i++) {
		if (m_rx_buf[i] == 0x0A) {
			line_len = i + 1;
			break;
		}
	}

	std::string str;

	if (line_len > 0) {
		for (uint32_t i = 0; i < line_len; i++) {
			str.push_back(m_rx_buf[i]);
		}

		uint32_t remaining = m_rx_len - line_len;

		for (uint32_t i = 0; i < remaining; i++) {
			m_rx_buf[i] = m_rx_buf[line_len + i];
		}

		m_rx_len = remaining;
	}

	return str;
}

bool Comms::rx_empty() const
{
	return m_rx_len == 0;
}

void Comms::reset_rx_buffer()
{
	m_rx_len = 0;
}
