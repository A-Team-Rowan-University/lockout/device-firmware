/*
 * Comms.h
 *
 * Author: Andrew Hollabaugh
 * Date Created: 8/23/20
 *
 * Handles serial (UART) communication
 * Contains buffer for received bytes
 */

#ifndef COMMS_H
#define COMMS_H

#include <string>
#include "usart.h"

class Comms {
public:
	//codes for gateway pi communication packets
	uint8_t TX_START_CHAR = 0x33; //start of debug info containing a packet when transmitted over radio
	uint8_t RX_START_CHAR = 0x55; //start of debug info containing a packet when recieved over radio
	uint8_t RX_DISC_START_CHAR = 0xee; //when packet was discarded due to radio packet filtering
	uint8_t GW_START_CHAR = 0xbb; //start char for when packet is sent to pi to transmit to server over websocket
	uint8_t COMM_ERR_CHAR = 0x66; //single char for when a communication error occurs
	uint8_t END_CHAR = 0xaa; //end of serial packet

	explicit Comms(UART_HandleTypeDef* pmtr_p_huart);
	void write(uint8_t *p_buf, uint16_t len);
	void write_str(std::string);
	void on_rx(uint8_t *p_buf, uint32_t len);
	bool rx_empty() const;
	void reset_rx_buffer();
	uint8_t read_char();
	std::string read_line();

private:
	UART_HandleTypeDef* m_p_huart;
	static const uint32_t TX_TIMEOUT = 100;
	static const uint32_t RX_BUF_LEN = 256; //max length of buffer
	volatile uint8_t m_rx_buf[RX_BUF_LEN] = {}; //buffer for received bytes
	volatile uint32_t m_rx_len; //current length of buffer
};

#endif /* COMMS_H */
