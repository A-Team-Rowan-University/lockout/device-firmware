/*
 * CardReader.h
 *
 * Author: Andrew Hollabaugh
 * Date Created: 6/26/20
 *
 * Application functionality for reading Rowan IDs using the MFRC522 library
 */

#ifndef CARD_READER_H
#define CARD_READER_H

#include "interface.h"

#include "MFRC522.h"
#include "card_key.h"
#include "Comms.h"

class CardReader {
public:
	explicit CardReader(MFRC522::CardReaderHWConf* pmtr_p_hw_conf, Comms* pmtr_p_debug_comms);

	void init();
    uint8_t* check_id();
    void print_id(uint8_t *p_id_buffer);

private:
	MFRC522::CardReaderHWConf* m_p_hw_conf;
	Comms* m_p_debug_comms;
	MFRC522 m_mfrc522;

    uint8_t* auth_read(MFRC522::MIFARE_Key *p_key);
    void error_handler(uint8_t error_code);
};

#endif //CARD_READER_H
