/*
 * interface.cpp
 *
 * Author: Andrew Hollabaugh
 * Date Created: 6/27/20
 *
 * Interface between target-specific hardware and application code
 * Fits LockoutDevice hw_conf struct with all the hardware LockoutDevice needs access to
 * Includes interrupt callbacks, which just call identical functions in LockoutDevice
 */

#include "interface.h"
#include "LockoutDevice.h"
#include "SPIRIT_Gpio.h"

static LockoutDevice *p_lockout_device;

LockoutDevice::DeviceHWConf device_hw_conf {
		.radio_hw_conf {
				.p_hspi = &hspi1,
				.NSS_GPIO_PORT = RADIO_NSS_GPIO_Port,
				.NSS_GPIO_PIN = RADIO_NSS_Pin,
				.SDN_GPIO_PORT = RADIO_SDN_GPIO_Port,
				.SDN_GPIO_PIN = RADIO_SDN_Pin,
				.INT_GPIO_PORT = RADIO_INT_GPIO_Port,
				.INT_GPIO_PIN = RADIO_INT_Pin,
				.INT_IRQ_HANDLER = RADIO_INT_EXTI_IRQn,
				.SPIRIT_INT_GPIO_PIN = SPIRIT_GPIO_3,
		},

		.card_reader_hw_conf {
				.p_hspi = &hspi2,
				.NSS_GPIO_PORT = PCD_SPI_NSS_GPIO_Port,
				.NSS_GPIO_PIN = PCD_SPI_NSS_Pin,
				.RST_GPIO_PORT = PCD_RST_GPIO_Port,
				.RST_GPIO_PIN = PCD_RST_Pin
		},

		.p_debug_comms_huart = &huart2,

		.LED_RX_GPIO_PORT = USER_LED_GPIO_Port,
		.LED_RX_GPIO_PIN = USER_LED_Pin,
		.p_rx_led_htim = &htim2,

		.LED_TX_GPIO_PORT = RADIO_LED_GPIO_Port,
		.LED_TX_GPIO_PIN = RADIO_LED_Pin,
		.p_tx_led_htim = &htim6,

		.BTN_PING_GPIO_PORT = USER_BUTTON_GPIO_Port,
		.BTN_PING_GPIO_PIN = USER_BUTTON_Pin
};

/*
 * Constructs a LockoutDevice and runs its main loop
 * Called from main.c
 */
void lockout_device_main()
{
	p_lockout_device = new LockoutDevice(&device_hw_conf);
    p_lockout_device->run();
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef* htim)
{
	if(htim->Instance == TIM2)
    {
	    p_lockout_device->on_rx_led_timer_overflow();
    }
	else if(htim->Instance == TIM6)
    {
	    p_lockout_device->on_tx_led_timer_overflow();
    }
}
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin==device_hw_conf.radio_hw_conf.INT_GPIO_PIN)
	{
		p_lockout_device->on_radio_int();
	}
	else if(GPIO_Pin==device_hw_conf.BTN_PING_GPIO_PIN)
	{
		p_lockout_device->on_ping_button_press();
	}
}
