/*
 * LCDMessages.cpp
 *
 * Author: Andrew Hollabaugh
 * Date Created: 6/4/21
 *
 * Contains all the messages that could be written to the LCD Display
 * Handles backlight colors for each message
 * Timeout function for displaying messages for a few seconds and going back to 'default' message
 */

#include "LCDMessages.h"

LCDMessages::LCDMessages(Display* pmtr_p_lcd, Buzzer* pmtr_p_buzzer)
{
	m_p_lcd = pmtr_p_lcd;
	m_p_buzzer = pmtr_p_buzzer;
}

/*
 * Set the buzzer beep sequences for the messages that use them
 * Cannot be done in msg_data struct initialization in header, because sometimes crashes,
 * probably since the Note arrays in Buzzer.h may not be initialized yet
 */
void LCDMessages::init_buzzer()
{
	msg_data[LCD_MSG_CONNECTION_ERROR].p_beep = m_p_buzzer->system_error;
	msg_data[LCD_MSG_CARD_READ_ERROR].p_beep = m_p_buzzer->system_error;
	msg_data[LCD_MSG_ACC_RESP_ERROR].p_beep = m_p_buzzer->system_error;
	msg_data[LCD_MSG_ACC_RESP_DENY].p_beep = m_p_buzzer->user_error;
	msg_data[LCD_MSG_CARD_CHECK_ERROR].p_beep = m_p_buzzer->user_error;
	msg_data[LCD_MSG_PB_FB].p_beep = m_p_buzzer->user_error;
}

/*
 * Display a message on the LCD
 * msg_name - a value of the lcd_msg_name enum corresponding to an element in msg_data
 */
void LCDMessages::display_msg(lcd_msg_name_t msg_name)
{
	if(msg_name != m_current_msg_name) //only update display if it isn't the same as before
	{
		lcd_msg_info_t* p_msg_info = &msg_data[msg_name];
		m_p_lcd->set_backlight(p_msg_info->bklt_red, p_msg_info->bklt_grn, p_msg_info->bklt_blu);
		m_p_lcd->write_str_double(p_msg_info->disp_top, p_msg_info->disp_bot);
		if(p_msg_info->use_timeout)
		{
			m_timeout_msg_start_time = HAL_GetTick();
		}
		if(p_msg_info->set_default)
		{
			m_current_default_msg_name = msg_name;
		}
		if(p_msg_info->p_beep != nullptr)
		{
			m_p_buzzer->play_beep(p_msg_info->p_beep);
		}
		m_current_msg_name = msg_name;
	}
}

/*
 * Display the default message if the timeout expires, only if a 'use_timeout' message is currently displayed
 */
void LCDMessages::update_timeout()
{
	if(msg_data[m_current_msg_name].use_timeout && \
		HAL_GetTick() - m_timeout_msg_start_time > MSG_TIMEOUT)
	{
		display_msg(m_current_default_msg_name);
	}
}

