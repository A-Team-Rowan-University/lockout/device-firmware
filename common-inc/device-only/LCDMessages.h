/*
 * LCDMessages.h
 *
 * Author: Andrew Hollabaugh
 * Date Created: 6/4/21
 *
 * Contains all the messages that could be written to the LCD Display
 * Handles backlight colors for each message
 * Timeout function for displaying messages for a few seconds and going back to 'default' message
 */

#ifndef LCDMESSAGES_H
#define LCDMESSAGES_H

#include <string>
#include "Display.h"
#include "Buzzer.h"

class LCDMessages {
public:
	typedef enum {
		LCD_MSG_NULL,
		LCD_MSG_INIT,
		LCD_MSG_CONNECTION_ERROR,
		LCD_MSG_IDLE,
		LCD_MSG_AUTH,
		LCD_MSG_CARD_READ_ERROR,
		LCD_MSG_ACC_RESP_ERROR,
		LCD_MSG_ACC_RESP_DENY,
		LCD_MSG_MCHN_ENABLE,
		LCD_MSG_MCHN_DISABLE,
		LCD_MSG_CARD_CHECK_ERROR,
		LCD_MSG_LOCK_MODE,
		LCD_MSG_ADMIN_ENABLE,
		LCD_MSG_PB_FB,
		LCD_MSG_HELP_BTN,
		LCD_MSG_REPORT_BTN,
		LCD_MSG_BROKEN_BTN,
		NUM_LCD_MSGS,
	} lcd_msg_name_t;

	lcd_msg_name_t m_current_msg_name = LCD_MSG_NULL;
	lcd_msg_name_t m_current_default_msg_name = LCD_MSG_NULL;
	lcd_msg_name_t m_idle_msg_name = LCD_MSG_IDLE; //changes to LOCK_MODE when in lock mode

	explicit LCDMessages(Display* pmtr_p_lcd, Buzzer* pmtr_p_buzzer);
	void init_buzzer();
	void display_msg(lcd_msg_name_t msg_name);
	void update_timeout();

private:
	Display* m_p_lcd;
	Buzzer* m_p_buzzer;

	const uint32_t MSG_TIMEOUT = 4000; //time for messages to time out that use 'use_timeout'
	uint32_t m_timeout_msg_start_time = 0;

	typedef struct {
		uint8_t bklt_red;
		uint8_t bklt_grn;
		uint8_t bklt_blu;
		std::string disp_top;
		std::string disp_bot;
		bool use_timeout; //message only displayed for a few seconds, then goes back to default message
		bool set_default; //use this message as the new default message
		Buzzer::Note* p_beep; //pointer to a Note array in Buzzer.h to play a beep sequence when message is displayed
	} lcd_msg_info_t;

	//contains all data for LCD messages - index of each defined by lcd_msg_name_t enum
	lcd_msg_info_t msg_data[NUM_LCD_MSGS] = {
			{0,     0,     0,     "",                       "",                     false,  false,  nullptr},
			{100,   60,    0,     "Initializing server ",   "communications...   ", false,  false,  nullptr},
			{100,   10,    10,    "Error: can't connect",   "to server. Retrying ", false,  false,  nullptr},
			{30,    20,    100,   " Insert ID Card to  ",   " enable the machine ", false,  true,   nullptr},
			{100,   60,    0,     " Authenticating...  ",   "                    ", false,  false,  nullptr},
			{100,   10,    10,    "Error: Could not    ",   "read ID. Try again  ", true,   false,  nullptr},
			{100,   10,    10,    "Error: Machine not  ",   "set up in server    ", true,   false,  nullptr},
			{100,   10,    0,     "   Access Denied    ",   "                    ", true,   false,  nullptr},
			{0,     100,   0,     "  Machine Enabled   ",   "Remove ID when done ", false,  true,   nullptr},
			{100,   10,    0,     "Disabling machine in",   "                    ", false,  false,  nullptr},
			{100,   10,    10,    "Error: ID check fail",   "                    ", true,   false,  nullptr},
			{100,   10,    0,     "   Machine Locked   ",   "                    ", false,  true,   nullptr},
			{0,     100,   100,   "  Machine Enabled   ",   "                    ", false,  true,   nullptr},
			{100,   10,    0,     " Enable inhibited   ",   "    by hardware     ", true,   false,  nullptr},
			{100,   5,     100,   "  Help report sent  ",   "                    ", true,   false,  nullptr},
			{100,   5,     100,   " Misuse report sent ",   "                    ", true,   false,  nullptr},
			{100,   5,     100,   "  Machine broken    ",   "   status sent      ", true,   false,  nullptr},
	};
};

#endif //LCDMESSAGES_H
