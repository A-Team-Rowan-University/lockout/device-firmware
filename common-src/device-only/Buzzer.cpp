/*
 * Buzzer.cpp
 *
 * Author: Andrew Hollabaugh
 * Date Created: 7/4/20
 *
 */

#include "Buzzer.h"

Buzzer::Buzzer(BuzzerHWConf* pmtr_p_hw_conf)
{
	m_p_hw_conf = pmtr_p_hw_conf;
	PWM_TIM_FREQ = m_p_hw_conf->CPU_FREQ / m_p_hw_conf->PWM_TIM_PRESCALER;
	NOTE_TIM_TICKS_PER_MS = m_p_hw_conf->CPU_FREQ / m_p_hw_conf->NOTE_TIM_PRESCALER / 1000;
}

/*
 * Play a sequence of tones
 * beep[] - array of Notes as defined in Buzzer.h
 */
void Buzzer::play_beep(Note* p_beep)
{
	if(m_p_current_note == nullptr)
	{
		m_p_current_note = &p_beep[0];
		play_note(&p_beep[0]);
		m_p_current_note++;
	}
}

/*
 * Play a single Note in a sequence
 * Resets note timer, sets buzzer frequency, starts note timer
 * p_note - pointer to Note object to play
 */
void Buzzer::play_note(Note* p_note)
{
    __HAL_TIM_SET_AUTORELOAD(m_p_hw_conf->p_htim_note,
		 p_note->length_ms * NOTE_TIM_TICKS_PER_MS);
    set_frequency(p_note->frequency);
    HAL_TIM_Base_Start_IT(m_p_hw_conf->p_htim_note);
}

/*
 * Called when note timer overflows via interrupt
 * Stops note timer, plays next note in sequence or stops if it is the end of the sequence
 */
void Buzzer::on_note_timer_overflow()
{
    HAL_TIM_Base_Stop_IT(m_p_hw_conf->p_htim_note);
	if(m_p_current_note->length_ms == 0) //is it the last note
    {
	    stop();
	    m_p_current_note = nullptr;
    }
	else
	{
		play_note(m_p_current_note++);
	}
}

/*
 * Set the frequency of the buzzer
 * frequency - float value of frequency in Hz, set to 0 to turn off buzzer
 */
void Buzzer::set_frequency(float frequency)
{
    if(frequency == 0)
    {
    	stop();
    }
    else
    {
        uint32_t timer_period = PWM_TIM_FREQ / frequency;
        __HAL_TIM_SET_AUTORELOAD(m_p_hw_conf->p_htim_pwm, timer_period);
        __HAL_TIM_SET_COMPARE(m_p_hw_conf->p_htim_pwm,
			m_p_hw_conf->PWM_CHANNEL, timer_period / 2);
        HAL_TIM_PWM_Start(m_p_hw_conf->p_htim_pwm, m_p_hw_conf->PWM_CHANNEL);
    }
}

void Buzzer::stop()
{
	HAL_TIM_PWM_Stop(m_p_hw_conf->p_htim_pwm, m_p_hw_conf->PWM_CHANNEL);
}


