/*
 * CardReader.cpp
 *
 * Author: Andrew Hollabaugh
 * Date Created: 6/26/20
 *
 * Application functionality for reading Rowan IDs using the MFRC522 library
 */

#include "CardReader.h"

CardReader::CardReader(MFRC522::CardReaderHWConf* pmtr_p_hw_conf, Comms* pmtr_p_debug_comms):
	m_mfrc522(pmtr_p_hw_conf, pmtr_p_debug_comms)
{
	m_p_hw_conf = pmtr_p_hw_conf;
	m_p_debug_comms = pmtr_p_debug_comms;
}

/*
 * Initialize MFRC522 IC
 */
void CardReader::init()
{
	m_mfrc522.PCD_Init();
}

/*
 * Attempt to read ID card
 * returns nullptr if read failed, otherwise returns pointer to the 16-byte id that was read
 */
uint8_t* CardReader::check_id()
{
    if(!(m_mfrc522.PICC_IsNewCardPresent() && m_mfrc522.PICC_ReadCardSerial()))
        return nullptr;

    MFRC522::MIFARE_Key key;

    //set the card key (authorization key to read contents of card)
    //if CARD_KEY doesn't exist, you need to obtain card_key.h from someone, this isn't in the repository for security purposes
    uint8_t card_key[MFRC522::MF_KEY_SIZE] = CARD_KEY;
    for(size_t i = 0; i < MFRC522::MF_KEY_SIZE; i++) //copy the card key to the instance variable in the MFRC library
        key.keyByte[i] = card_key[i];

    uint8_t* id_buffer  = auth_read(&key);

    m_mfrc522.PCD_Init();

    return id_buffer;
}

/*
 * Read the ID card using the provided authentication key, directly using the MFRC library
 * p_key - pointer to the key to access the contents of the ID card
 * returns nullptr if read unsuccessful - otherwise return pointer to ID that was read
 */
uint8_t* CardReader::auth_read(MFRC522::MIFARE_Key* p_key)
{
    const uint8_t id_block = 8;

    MFRC522::StatusCode status;
    status = m_mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, id_block, p_key, &(m_mfrc522.uid));
    if(status != MFRC522::STATUS_OK)
    {
        error_handler(0);
        return nullptr;
    }

    static uint8_t buffer[18];
    uint8_t byte_count = sizeof(buffer);
    status = m_mfrc522.MIFARE_Read(id_block, buffer, &byte_count);
    m_mfrc522.PICC_HaltA();       // Halt PICC
    m_mfrc522.PCD_StopCrypto1();  // Stop encryption on PCD
    if(status != MFRC522::STATUS_OK)
    {
        error_handler(1);
        return nullptr;
    }

    return buffer;
}

/*
 * Print an ID number to the serial port in ASCII
 */
void CardReader::print_id(uint8_t *p_id_buffer)
{
    m_p_debug_comms->write_str("id: ");
    for(uint8_t i = 0; i < 16; i++)
    {
    	m_p_debug_comms->write(&p_id_buffer[i], 1);
    }
    m_p_debug_comms->write_str("\n");
}

void CardReader::error_handler(uint8_t error_code)
{
    switch(error_code)
    {
        case 0: m_p_debug_comms->write_str("auth fail\n");
        case 1: m_p_debug_comms->write_str("read fail\n");
    }
}

