/*
 * RadioLowLevel.cpp
 *
 * Author: Andrew Hollabaugh
 * Date Created: 8/6/20
 */

#include "RadioLowLevel.h"

//instantiate a RadioHWConf outside of the class
RadioLowLevel::RadioHWConf* RadioLowLevel::m_p_hw_conf = {};

RadioLowLevel::RadioLowLevel(RadioHWConf* pmtr_p_hw_conf)
{
	m_p_hw_conf = pmtr_p_hw_conf;
}

StatusBytes RadioSpiWriteRegisters(uint8_t cRegAddress, uint8_t cNbBytes, uint8_t* pcBuffer)
{
	uint8_t aHeader[2] = {0};
	uint16_t tmpstatus = 0x0000;

	StatusBytes *pStatus=(StatusBytes *)&tmpstatus;

	/* Built the aHeader bytes */
	aHeader[0] = WRITE_HEADER;
	aHeader[1] = cRegAddress;

	SPI_ENTER_CRITICAL();

	/* Puts the SPI chip select low to start the transaction */
	RadioSpiCSLow();

	for (volatile uint16_t Index = 0; Index < CS_TO_SCLK_DELAY; Index++);

	/* Write the aHeader bytes and read the SPIRIT1 status bytes */
	HAL_SPI_TransmitReceive(RadioLowLevel::m_p_hw_conf->p_hspi, (uint8_t *)&aHeader[0], (uint8_t *)&(tmpstatus), 1, RadioLowLevel::SPI_TIMEOUT);
	tmpstatus = tmpstatus << 8;

	/* Write the aHeader bytes and read the SPIRIT1 status bytes */
	HAL_SPI_TransmitReceive(RadioLowLevel::m_p_hw_conf->p_hspi, (uint8_t *)&aHeader[1], (uint8_t *)&tmpstatus, 1, RadioLowLevel::SPI_TIMEOUT);

	/* Writes the registers according to the number of bytes */
	for (int index = 0; index < cNbBytes; index++)
	{
		SPI_Write(pcBuffer[index]);
	}

	/* To be sure to don't rise the Chip Select before the end of last sending */
	while (__HAL_SPI_GET_FLAG(RadioLowLevel::m_p_hw_conf->p_hspi, SPI_FLAG_TXE) == RESET);
	/* Puts the SPI chip select high to end the transaction */
	RadioSpiCSHigh();

	SPI_EXIT_CRITICAL();

	return *pStatus;
}

StatusBytes RadioSpiReadRegisters(uint8_t cRegAddress, uint8_t cNbBytes, uint8_t* pcBuffer)
{
	uint16_t tmpstatus = 0x00;
	StatusBytes *pStatus = (StatusBytes *)&tmpstatus;

	uint8_t aHeader[2] = {0};
	uint8_t dummy = 0xFF;

	/* Built the aHeader bytes */
	aHeader[0] = READ_HEADER;
	aHeader[1] = cRegAddress;

	SPI_ENTER_CRITICAL();

	/* Put the SPI chip select low to start the transaction */
	RadioSpiCSLow();

	for (volatile uint16_t Index = 0; Index < CS_TO_SCLK_DELAY; Index++);

	/* Write the aHeader bytes and read the SPIRIT1 status bytes */
	HAL_SPI_TransmitReceive(RadioLowLevel::m_p_hw_conf->p_hspi, (uint8_t *)&aHeader[0], (uint8_t *)&(tmpstatus), 1, RadioLowLevel::SPI_TIMEOUT);
	tmpstatus = tmpstatus << 8;

	/* Write the aHeader bytes and read the SPIRIT1 status bytes */
	HAL_SPI_TransmitReceive(RadioLowLevel::m_p_hw_conf->p_hspi, (uint8_t *)&aHeader[1], (uint8_t *)&tmpstatus, 1, RadioLowLevel::SPI_TIMEOUT);

	for (int index = 0; index < cNbBytes; index++)
	{
		HAL_SPI_TransmitReceive(RadioLowLevel::m_p_hw_conf->p_hspi, (uint8_t *)&dummy, (uint8_t *)&(pcBuffer)[index], 1, RadioLowLevel::SPI_TIMEOUT);
	}

	/* To be sure to don't rise the Chip Select before the end of last sending */
	while (__HAL_SPI_GET_FLAG(RadioLowLevel::m_p_hw_conf->p_hspi, SPI_FLAG_TXE) == RESET);

	/* Put the SPI chip select high to end the transaction */
	RadioSpiCSHigh();

	SPI_EXIT_CRITICAL();

	return *pStatus;
}

StatusBytes RadioSpiCommandStrobes(uint8_t cCommandCode)
{
	uint8_t aHeader[2] = {0};
	uint16_t tmpstatus = 0x0000;

	StatusBytes *pStatus = (StatusBytes *)&tmpstatus;

	/* Built the aHeader bytes */
	aHeader[0] = COMMAND_HEADER;
	aHeader[1] = cCommandCode;

	SPI_ENTER_CRITICAL();

	/* Puts the SPI chip select low to start the transaction */
	RadioSpiCSLow();

	for (volatile uint16_t Index = 0; Index < CS_TO_SCLK_DELAY; Index++);
	/* Write the aHeader bytes and read the SPIRIT1 status bytes */
	HAL_SPI_TransmitReceive(RadioLowLevel::m_p_hw_conf->p_hspi, (uint8_t *)&aHeader[0], (uint8_t *)&tmpstatus, 1, RadioLowLevel::SPI_TIMEOUT);
	tmpstatus = tmpstatus<<8;

	/* Write the aHeader bytes and read the SPIRIT1 status bytes */
	HAL_SPI_TransmitReceive(RadioLowLevel::m_p_hw_conf->p_hspi, (uint8_t *)&aHeader[1], (uint8_t *)&tmpstatus, 1, RadioLowLevel::SPI_TIMEOUT);

	/* To be sure to don't rise the Chip Select before the end of last sending */
	while (__HAL_SPI_GET_FLAG(RadioLowLevel::m_p_hw_conf->p_hspi, SPI_FLAG_TXE) == RESET);

	/* Puts the SPI chip select high to end the transaction */
	RadioSpiCSHigh();

	SPI_EXIT_CRITICAL();

	return *pStatus;
}

StatusBytes RadioSpiWriteFifo(uint8_t cNbBytes, uint8_t* pcBuffer)
{
	uint16_t tmpstatus = 0x0000;
	StatusBytes *pStatus = (StatusBytes *)&tmpstatus;

	uint8_t aHeader[2] = {0};

	/* Built the aHeader bytes */
	aHeader[0] = WRITE_HEADER;
	aHeader[1] = LINEAR_FIFO_ADDRESS;

	SPI_ENTER_CRITICAL();

	/* Put the SPI chip select low to start the transaction */
	RadioSpiCSLow();

	for (volatile uint16_t Index = 0; Index < CS_TO_SCLK_DELAY; Index++);

	/* Write the aHeader bytes and read the SPIRIT1 status bytes */
	HAL_SPI_TransmitReceive(RadioLowLevel::m_p_hw_conf->p_hspi, (uint8_t *)&aHeader[0], (uint8_t *)&tmpstatus, 1, RadioLowLevel::SPI_TIMEOUT);
	tmpstatus = tmpstatus<<8;

	/* Write the aHeader bytes and read the SPIRIT1 status bytes */
	HAL_SPI_TransmitReceive(RadioLowLevel::m_p_hw_conf->p_hspi, (uint8_t *)&aHeader[1], (uint8_t *)&tmpstatus, 1, RadioLowLevel::SPI_TIMEOUT);

	/* Writes the registers according to the number of bytes */
	for (int index = 0; index < cNbBytes; index++)
	{
		SPI_Write(pcBuffer[index]);
	}

	/* To be sure to don't rise the Chip Select before the end of last sending */
	while (__HAL_SPI_GET_FLAG(RadioLowLevel::m_p_hw_conf->p_hspi, SPI_FLAG_TXE) == RESET);

	/* Put the SPI chip select high to end the transaction */
	RadioSpiCSHigh();

	SPI_EXIT_CRITICAL();

	return *pStatus;
}

StatusBytes RadioSpiReadFifo(uint8_t cNbBytes, uint8_t* pcBuffer)
{
	uint16_t tmpstatus = 0x0000;
	StatusBytes *pStatus = (StatusBytes *)&tmpstatus;

	uint8_t aHeader[2];
	uint8_t dummy=0xFF;

	/* Built the aHeader bytes */
	aHeader[0]=READ_HEADER;
	aHeader[1]=LINEAR_FIFO_ADDRESS;

	SPI_ENTER_CRITICAL();

	/* Put the SPI chip select low to start the transaction */
	RadioSpiCSLow();

	for (volatile uint16_t Index = 0; Index < CS_TO_SCLK_DELAY; Index++);

	/* Write the aHeader bytes and read the SPIRIT1 status bytes */
	HAL_SPI_TransmitReceive(RadioLowLevel::m_p_hw_conf->p_hspi, (uint8_t *)&aHeader[0], (uint8_t *)&tmpstatus, 1, RadioLowLevel::SPI_TIMEOUT);
	tmpstatus = tmpstatus<<8;

	/* Write the aHeader bytes and read the SPIRIT1 status bytes */
	HAL_SPI_TransmitReceive(RadioLowLevel::m_p_hw_conf->p_hspi, (uint8_t *)&aHeader[1], (uint8_t *)&tmpstatus, 1, RadioLowLevel::SPI_TIMEOUT);

	for (int index = 0; index < cNbBytes; index++)
	{
		HAL_SPI_TransmitReceive(RadioLowLevel::m_p_hw_conf->p_hspi, (uint8_t *)&dummy, (uint8_t *)&pcBuffer[index], 1, RadioLowLevel::SPI_TIMEOUT);
	}

	/* To be sure to don't rise the Chip Select before the end of last sending */
	while(__HAL_SPI_GET_FLAG(RadioLowLevel::m_p_hw_conf->p_hspi, SPI_FLAG_TXE) == RESET);

	/* Put the SPI chip select high to end the transaction */
	RadioSpiCSHigh();

	SPI_EXIT_CRITICAL();

	return *pStatus;
}

void RadioEnterShutdown()
{
	HAL_GPIO_WritePin(RadioLowLevel::m_p_hw_conf->SDN_GPIO_PORT, RadioLowLevel::m_p_hw_conf->SDN_GPIO_PIN, GPIO_PIN_SET);
}

void RadioExitShutdown()
{
	HAL_GPIO_WritePin(RadioLowLevel::m_p_hw_conf->SDN_GPIO_PORT, RadioLowLevel::m_p_hw_conf->SDN_GPIO_PIN, GPIO_PIN_RESET);

	/* Delay to allow the circuit POR, about 700 us */
	for (volatile uint32_t Index = 0; Index < POR_TIME; Index++);
}

SpiritFlagStatus RadioCheckShutdown()
{
	return (SpiritFlagStatus)(HAL_GPIO_ReadPin(RadioLowLevel::m_p_hw_conf->SDN_GPIO_PORT, RadioLowLevel::m_p_hw_conf->SDN_GPIO_PIN));
}

void RadioGpioInterruptCmd(IRQn_Type IRQ_HANDLER, uint8_t nPreemption, uint8_t nSubpriority, FunctionalState xNewState)
{
	HAL_NVIC_SetPriority(IRQ_HANDLER, nPreemption, nSubpriority);
	if (!xNewState)
	{
		HAL_NVIC_DisableIRQ(IRQ_HANDLER);
	}
	else
	{
		HAL_NVIC_EnableIRQ(IRQ_HANDLER);
	}
}

static void SPI_Write(uint8_t Value)
{
	HAL_StatusTypeDef status = HAL_OK;

	while (__HAL_SPI_GET_FLAG(RadioLowLevel::m_p_hw_conf->p_hspi, SPI_FLAG_TXE) == RESET);
	status = HAL_SPI_Transmit(RadioLowLevel::m_p_hw_conf->p_hspi, (uint8_t*) &Value, 1, RadioLowLevel::SPI_TIMEOUT);

	/* Check the communication status */
	if (status != HAL_OK)
	{
		/* Execute user timeout callback */
		SPI_Error();
	}
}

static void SPI_Error()
{
	/* De-initialize the SPI communication BUS */
	HAL_SPI_DeInit(RadioLowLevel::m_p_hw_conf->p_hspi);

	/* Re-Initiaize the SPI communication BUS */
	HAL_SPI_Init(RadioLowLevel::m_p_hw_conf->p_hspi);
}
