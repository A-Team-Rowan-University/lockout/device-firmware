/*
 * Buzzer.h
 *
 * Author: Andrew Hollabaugh
 * Date Created: 7/4/20
 *
 * Play sequences of frequencies on the buzzer
 */

#ifndef BUZZER_H
#define BUZZER_H

#include "interface.h"

class Buzzer {
public:
    struct Note {
        float frequency;
        uint16_t length_ms;
    };

	Note startup[5] = {
			{659.25,     50},
			{698.46,     50},
			{783.99,     50},
			{880.00,     50},
			{0, 0},
	};

	Note enable[4] = {
			{783.99,     150},
			{880.00,     150},
			{987.77,     150},
			{0, 0},
	};

	Note disable[4] = {
			{987.77,     150},
			{880.00,     150},
			{783.99,     150},
			{0, 0},
	};

	Note user_error[5] = {
			{880.00,     250},
			{0,          100},
			{880.00,     250},
			{0, 0},
	};

	Note system_error[5] = {
			{987.77,     150},
			{0,          100},
			{987.77,     150},
			{0, 0},
	};

	Note* m_p_current_note = nullptr;

	struct BuzzerHWConf {
		TIM_HandleTypeDef* p_htim_pwm;
		TIM_HandleTypeDef* p_htim_note;
		const uint8_t PWM_CHANNEL;
		const uint32_t CPU_FREQ;
		const uint32_t PWM_TIM_PRESCALER;
		const uint32_t NOTE_TIM_PRESCALER;
	};

	explicit Buzzer(BuzzerHWConf* pmtr_p_hw_conf);

    void play_beep(Note* p_beep);
    void play_note(Note* p_note);
    void on_note_timer_overflow();
    void set_frequency(float frequency);
    void stop();

private:
	BuzzerHWConf* m_p_hw_conf;
	uint32_t PWM_TIM_FREQ;
	float NOTE_TIM_TICKS_PER_MS;
};

#endif //BUZZER_H
