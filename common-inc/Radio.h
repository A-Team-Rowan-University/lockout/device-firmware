/*
 * Radio.h
 *
 * Author: Andrew Hollabaugh
 * Date Created: 6/27/20
 *
 * Application functionality for using the SPIRIT1 radio via the SPIRIT1 X-CUBE-SUBG1 library
 */

#ifndef RADIO_H
#define RADIO_H

#include "interface.h"
#include "RadioLowLevel.h"
#include "SPIRIT_Config.h"
#include "Comms.h"
#include "aes_key.h"

class Radio {
public:
	//flags set by interrupt
	volatile bool tx_done = false;
	volatile bool rx_done = false;
	volatile bool rx_timeout = false;

	uint8_t GATEWAY_ADDR = 0x01;
	uint8_t UNASSIGNED_ADDR = 0xff;
	static const size_t PAYLOAD_LEN = 20;
	static const size_t PAYLOAD_DATA_LEN = 16;

	typedef enum {
		CMD_NULL        = 0x00,
		CMD_PING        = 0x01,
		CMD_ADDR_REQ    = 0x02, //address request
		CMD_ADDR_PROV   = 0x03, //adress provide
		CMD_ACC_REQ     = 0x10, //access request
		CMD_ACC_RESP    = 0x11, //access response
		CMD_MCHN_CTRL   = 0x12, //machine control
		CMD_MCHN_STATE  = 0x13, //machine state
		CMD_USER_BUTTON = 0x14,
		CMD_LOCK_MODE   = 0x15, //set lock mode
		CMD_CLEAR_CACHE = 0x16,
		CMD_ACK         = 0xff
	} cmd_t;

	typedef struct
	{
		cmd_t cmd;
		uint8_t source_addr;
		uint8_t tag;
		uint8_t retry;
		uint8_t data[PAYLOAD_DATA_LEN];
	} payload_t;

	explicit Radio(Comms* pmtr_p_comms);
	void init();
	void set_addr(uint8_t new_addr);
	void start_tx(uint8_t addr, payload_t* payload);
	void start_rx(float timeout);
	bool get_rx_payload(payload_t* rx_payload);
	void aes_use(bool enc_dec, uint8_t* start_buf, uint8_t* end_buf);
	void print_payload(uint8_t* p_start_char, Radio::payload_t* p_payload,
	                   uint8_t* p_dest_addr, uint8_t* p_rssi);
	void on_radio_int();

private:
	Comms* m_p_comms;

	//same as PAYLOAD_LEN, but next largest multiple of 16, because AES works in multiples of 16
	static const size_t PAYLOAD_LEN_FULL = 32;

	volatile bool aes_end = false; //done with aes operation

	//key for aes encryption
	//if LOCKOUT_AES_KEY does not exist, you need to obtain aes_key.h from someone, this isn't in the repository for security purposes
	uint8_t aes_key[16] = LOCKOUT_AES_KEY;

	static const uint32_t XTAL_FREQUENCY = 50000000; //crystal frequency: 50MHz
	static const uint8_t POWER_INDEX = 7;
	constexpr static const float POWER_DBM = 11.6;
	static const int32_t RSSI_THRESHOLD = -120; //default RSSI at reception, more than noise floor
	static const int32_t CSMA_RSSI_THRESHOLD = -90; //higher RSSI to transmit. If it's lower, the Channel will be seen as busy

	//radio operation settings - see radio library for more information
	SGpioInit GpioInit = {
		.xSpiritGpioPin = RadioLowLevel::m_p_hw_conf->SPIRIT_INT_GPIO_PIN,
		.xSpiritGpioMode = SPIRIT_GPIO_MODE_DIGITAL_OUTPUT_LP,
		.xSpiritGpioIO = SPIRIT_GPIO_DIG_OUT_IRQ
	};
	SRadioInit RadioInit = {
		.nXtalOffsetPpm     = 0,
		.lFrequencyBase     = 915000000,
		.nChannelSpace      = 100000,
		.cChannelNumber     = 0,
		.xModulationSelect  = FSK,
		.lDatarate          = 38400,
		.lFreqDev           = 20000,
		.lBandwidth         = 100000
	};
	PktBasicInit BasicPktInit {
		.xPreambleLength    = PKT_PREAMBLE_LENGTH_04BYTES,
		.xSyncLength        = PKT_SYNC_LENGTH_4BYTES,
		.lSyncWords         = 0x88888888,
		.xFixVarLength      = PKT_LENGTH_VAR,
		.cPktLengthWidth    = 7,
		.xCrcMode           = PKT_CRC_MODE_8BITS,
		.xControlLength     = PKT_CONTROL_LENGTH_0BYTES,
		.xAddressField      = S_ENABLE,
		.xFec               = S_DISABLE,
		.xDataWhitening     = S_ENABLE
	};
	PktBasicAddressesInit BasicPktAddressesInit {
		.xFilterOnMyAddress         = S_ENABLE,
		.cMyAddress                 = 0x12,
		.xFilterOnMulticastAddress  = S_ENABLE,
		.cMulticastAddress          = 0xEE,
		.xFilterOnBroadcastAddress  = S_ENABLE,
		.cBroadcastAddress          = 0xFF
	};
	CsmaInit CSMAInit {
		.xCsmaPersistentMode    = S_DISABLE,
		.xMultiplierTbit        = TBIT_TIME_64,
		.xCcaLength             = TCCA_TIME_3,
		.cMaxNb                 = 5,
		.nBuCounterSeed         = 0xFA21,
		.cBuPrescaler           = 32
	};
	SpiritIrqs irq_status = {};

	static void enable_sqi();
};

#endif //RADIO_H
