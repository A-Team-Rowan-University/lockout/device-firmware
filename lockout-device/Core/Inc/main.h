/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LED0_Pin GPIO_PIN_13
#define LED0_GPIO_Port GPIOC
#define RCC_NE0_Pin GPIO_PIN_14
#define RCC_NE0_GPIO_Port GPIOC
#define RCC_NE1_Pin GPIO_PIN_15
#define RCC_NE1_GPIO_Port GPIOC
#define BTN0_Pin GPIO_PIN_0
#define BTN0_GPIO_Port GPIOF
#define BTN1_Pin GPIO_PIN_1
#define BTN1_GPIO_Port GPIOF
#define BTN1_EXTI_IRQn EXTI0_1_IRQn
#define EXT_LED_Pin GPIO_PIN_0
#define EXT_LED_GPIO_Port GPIOA
#define KEY_DISABLE_Pin GPIO_PIN_1
#define KEY_DISABLE_GPIO_Port GPIOA
#define EEPROM_WP_Pin GPIO_PIN_2
#define EEPROM_WP_GPIO_Port GPIOA
#define RADIO_GPIO2_Pin GPIO_PIN_3
#define RADIO_GPIO2_GPIO_Port GPIOA
#define RADIO_CSN_Pin GPIO_PIN_4
#define RADIO_CSN_GPIO_Port GPIOA
#define RADIO_SCK_Pin GPIO_PIN_5
#define RADIO_SCK_GPIO_Port GPIOA
#define RADIO_MISO_Pin GPIO_PIN_6
#define RADIO_MISO_GPIO_Port GPIOA
#define RADIO_MOSI_Pin GPIO_PIN_7
#define RADIO_MOSI_GPIO_Port GPIOA
#define RADIO_INT_Pin GPIO_PIN_0
#define RADIO_INT_GPIO_Port GPIOB
#define RADIO_INT_EXTI_IRQn EXTI0_1_IRQn
#define RADIO_SDN_Pin GPIO_PIN_1
#define RADIO_SDN_GPIO_Port GPIOB
#define KEY_ENABLE_Pin GPIO_PIN_2
#define KEY_ENABLE_GPIO_Port GPIOB
#define MCHN_EN_Pin GPIO_PIN_10
#define MCHN_EN_GPIO_Port GPIOB
#define PB_FB_Pin GPIO_PIN_11
#define PB_FB_GPIO_Port GPIOB
#define PCD_RST_Pin GPIO_PIN_12
#define PCD_RST_GPIO_Port GPIOB
#define PCD_SCK_Pin GPIO_PIN_13
#define PCD_SCK_GPIO_Port GPIOB
#define PCD_MISO_Pin GPIO_PIN_14
#define PCD_MISO_GPIO_Port GPIOB
#define PCD_MOSI_Pin GPIO_PIN_15
#define PCD_MOSI_GPIO_Port GPIOB
#define LCD_RED_Pin GPIO_PIN_8
#define LCD_RED_GPIO_Port GPIOA
#define UART_TX_Pin GPIO_PIN_9
#define UART_TX_GPIO_Port GPIOA
#define CARD_INS_SW_Pin GPIO_PIN_6
#define CARD_INS_SW_GPIO_Port GPIOC
#define PCD_NSS_Pin GPIO_PIN_7
#define PCD_NSS_GPIO_Port GPIOC
#define UART_RX_Pin GPIO_PIN_10
#define UART_RX_GPIO_Port GPIOA
#define EEPROM_SCL_Pin GPIO_PIN_11
#define EEPROM_SCL_GPIO_Port GPIOA
#define EEPROM_SDA_Pin GPIO_PIN_12
#define EEPROM_SDA_GPIO_Port GPIOA
#define SWDIO_Pin GPIO_PIN_13
#define SWDIO_GPIO_Port GPIOA
#define SWCLK_Pin GPIO_PIN_14
#define SWCLK_GPIO_Port GPIOA
#define LED1_Pin GPIO_PIN_15
#define LED1_GPIO_Port GPIOA
#define BZR_Pin GPIO_PIN_0
#define BZR_GPIO_Port GPIOD
#define LED2_Pin GPIO_PIN_1
#define LED2_GPIO_Port GPIOD
#define LED3_Pin GPIO_PIN_2
#define LED3_GPIO_Port GPIOD
#define EXT_BTN0_Pin GPIO_PIN_3
#define EXT_BTN0_GPIO_Port GPIOD
#define LCD_GRN_Pin GPIO_PIN_3
#define LCD_GRN_GPIO_Port GPIOB
#define EXT_BTN1_Pin GPIO_PIN_4
#define EXT_BTN1_GPIO_Port GPIOB
#define EXT_BTN2_Pin GPIO_PIN_5
#define EXT_BTN2_GPIO_Port GPIOB
#define LCD_BLU_Pin GPIO_PIN_6
#define LCD_BLU_GPIO_Port GPIOB
#define LCD_RST_Pin GPIO_PIN_7
#define LCD_RST_GPIO_Port GPIOB
#define LCD_SCL_Pin GPIO_PIN_8
#define LCD_SCL_GPIO_Port GPIOB
#define LCD_SDA_Pin GPIO_PIN_9
#define LCD_SDA_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
