/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define USER_BUTTON_Pin GPIO_PIN_13
#define USER_BUTTON_GPIO_Port GPIOC
#define USER_BUTTON_EXTI_IRQn EXTI4_15_IRQn
#define UART_DB_Pin GPIO_PIN_2
#define UART_DB_GPIO_Port GPIOA
#define UART_DBA3_Pin GPIO_PIN_3
#define UART_DBA3_GPIO_Port GPIOA
#define USER_LED_Pin GPIO_PIN_5
#define USER_LED_GPIO_Port GPIOA
#define RADIO_MISO_Pin GPIO_PIN_6
#define RADIO_MISO_GPIO_Port GPIOA
#define RADIO_MOSI_Pin GPIO_PIN_7
#define RADIO_MOSI_GPIO_Port GPIOA
#define PCD_RST_Pin GPIO_PIN_2
#define PCD_RST_GPIO_Port GPIOB
#define PCD_SPI_NSS_Pin GPIO_PIN_12
#define PCD_SPI_NSS_GPIO_Port GPIOB
#define PCD_SPI_SCK_Pin GPIO_PIN_13
#define PCD_SPI_SCK_GPIO_Port GPIOB
#define PCD_SPI_MISO_Pin GPIO_PIN_14
#define PCD_SPI_MISO_GPIO_Port GPIOB
#define PCD_SPI_MOSI_Pin GPIO_PIN_15
#define PCD_SPI_MOSI_GPIO_Port GPIOB
#define RADIO_INT_Pin GPIO_PIN_7
#define RADIO_INT_GPIO_Port GPIOC
#define RADIO_INT_EXTI_IRQn EXTI4_15_IRQn
#define RADIO_SDN_Pin GPIO_PIN_10
#define RADIO_SDN_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define RADIO_SCK_Pin GPIO_PIN_3
#define RADIO_SCK_GPIO_Port GPIOB
#define RADIO_LED_Pin GPIO_PIN_4
#define RADIO_LED_GPIO_Port GPIOB
#define RADIO_NSS_Pin GPIO_PIN_6
#define RADIO_NSS_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
