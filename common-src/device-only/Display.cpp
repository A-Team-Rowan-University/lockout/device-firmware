/*
 * Display.cpp
 *
 * Author: Andrew Hollabaugh
 * Date Created: 7/1/20
 *
 * Driver for LCD Character display using the ST7036 driver IC and I2C interface
 * Use with 'NHD-C0220BiZ-FSRGB-FBW-3V3' display
 */

#include "Display.h"

Display::Display(DisplayHWConf* pmtr_p_hw_conf)
{
	m_p_hw_conf = pmtr_p_hw_conf;
}

/*
 * Initializes LCD - reset and set some default register values
 */
void Display::init()
{
	HAL_GPIO_WritePin(m_p_hw_conf->RST_GPIO_PORT, m_p_hw_conf->RST_GPIO_PIN, GPIO_PIN_SET);
	HAL_Delay(RST_DELAY);
	HAL_GPIO_WritePin(m_p_hw_conf->RST_GPIO_PORT, m_p_hw_conf->RST_GPIO_PIN, GPIO_PIN_RESET);
	HAL_Delay(RST_DELAY);
	HAL_GPIO_WritePin(m_p_hw_conf->RST_GPIO_PORT, m_p_hw_conf->RST_GPIO_PIN, GPIO_PIN_SET);
	HAL_Delay(RST_DELAY);

	write_instruction(FUNCTION_SET_DATA_LENGTH_8BIT | \
        FUNCTION_SET_DOUBLE_LINE | FUNCTION_SET_EXTENDED_INSTRUCTION);
    write_instruction(ENTRY_MODE_SHIFT_CURSOR_RIGHT);
	write_instruction(BIAS_OSC_BIAS_1_4 | BIAS_OSC_FREQ_183);
	set_contrast(CONTRAST_DEFAULT_VAL);
    write_instruction(DISPLAY_MODE_MASK | DISPLAY_MODE_DISPLAY_ON | \
        DISPLAY_MODE_CURSOR_OFF | DISPLAY_MODE_BLINK_OFF);
}

/*
 * Set all 3 RGB backlight channels
 * xxx_percent - 0 to 100 brightness value for each of 3 channels
 */
void Display::set_backlight(uint8_t red_percent, uint8_t grn_percent, uint8_t blu_percent)
{
	set_backlight_channel(m_p_hw_conf->BKLT_RED_PWM_CHANNEL, red_percent);
	set_backlight_channel(m_p_hw_conf->BKLT_GRN_PWM_CHANNEL, grn_percent);
	set_backlight_channel(m_p_hw_conf->BKLT_BLU_PWM_CHANNEL, blu_percent);
}

/*
 * Set a single channel of the RGB backlight
 * pwm_channel - PWM_CHANNEL from hw_conf
 * brightness_percent - brightness value from 0 to 100
 */
void Display::set_backlight_channel(uint8_t pwm_channel, uint8_t brightness_percent)
{
    uint32_t timer_period = __HAL_TIM_GET_AUTORELOAD(m_p_hw_conf->p_bklt_htim);
    uint32_t pwm_compare_val = (uint16_t)(((float)(brightness_percent) / 100.0) * timer_period);
    __HAL_TIM_SET_COMPARE(m_p_hw_conf->p_bklt_htim, pwm_channel, pwm_compare_val);
    HAL_TIM_PWM_Start(m_p_hw_conf->p_bklt_htim, pwm_channel);
}

/*
 * Set the contrast of the display text
 * contrast_val - between 0x00 and 0x3f
 */
void Display::set_contrast(uint8_t contrast_val)
{
    write_instruction(FUNCTION_SET_DATA_LENGTH_8BIT | \
        FUNCTION_SET_DOUBLE_LINE| FUNCTION_SET_EXTENDED_INSTRUCTION);
    write_instruction(FOLLOWER_CONTROL_FOLLOWER_ON | RAB_VAL);
    if(contrast_val > CONTRAST_MAX_VAL) contrast_val = CONTRAST_MAX_VAL;
    write_instruction(ICON_CONTROL_ICON_ON | ICON_CONTROL_BOOST_ON | (contrast_val >> 4));
    write_instruction(CONTRAST_SET_LOW | (contrast_val & 0x0f));
}

/*
 * Write a line to the LCD
 * p_line - pointer to line of characters to write
 * len - number of characters to write, max 20
 * line_type - value of line_type_t to use - top or bottom line, or double-height
 */
void Display::write(uint8_t* p_line, size_t len, line_type_t line_type)
{
	switch(line_type)
	{
		case LT_TOP:
		{
			write_instruction(FUNCTION_SET_DATA_LENGTH_8BIT | \
				FUNCTION_SET_DOUBLE_LINE | FUNCTION_SET_NORMAL_INSTRUCTION);
			write_instruction(SET_DDRAM_ADDR | 0x00);
			break;
		}
		case LT_BOTTOM:
		{
			write_instruction(FUNCTION_SET_DATA_LENGTH_8BIT | \
				FUNCTION_SET_DOUBLE_LINE | FUNCTION_SET_NORMAL_INSTRUCTION);
			write_instruction(SET_DDRAM_ADDR | BOTTOM_LINE_BEGIN_ADDR);
			break;
		}
		case LT_DOUBLE_HEIGHT:
		{
			write_instruction(FUNCTION_SET_DATA_LENGTH_8BIT | \
				FUNCTION_SET_SINGLE_LINE_DOUBLE_HEIGHT | FUNCTION_SET_NORMAL_INSTRUCTION);
			write_instruction(SET_DDRAM_ADDR | 0x00);
			break;
		}
	}
	write_data(p_line, len);
}

/*
 * Write a line to the LCD using std:string
 * str - string to write to LCD, max 20 chars
 * line_type - value of line_type_t to use - top or bottom line, or double-height
 */
void Display::write_str(std::string str, line_type_t line_type)
{
	auto* p_data = (uint8_t*) str.c_str();
	write(p_data, str.length(), line_type);
}

/*
 * Write both the top and bottom lines of the LCD from std:strings
 * str_top - chracters for top line
 * str_bot - chracters for bottom line
 */
void Display::write_str_double(std::string str_top, std::string str_bot)
{
	write_str(str_top, LT_TOP);
	write_str(str_bot, LT_BOTTOM);
}

/*
 * Clear the display
 */
void Display::clear()
{
    write_instruction(CLEAR_DISPLAY);
}

/*
 * Write a single instruction to the ST7036 driver
 * inst - instruction byte to write, one of the enums in Display.h
 */
void Display::write_instruction(uint8_t inst)
{
    uint8_t control_byte = 0x80; //Co = 1, RS = 0
    uint8_t buffer[2] = {control_byte, inst};
    i2c_transmit(buffer, 2);
    HAL_Delay(WRITE_DELAY);
}

/*
 * Write data to the ST7036 driver's memory
 * p_data - pointer to data to write
 * len - number of bytes to write
 */
void Display::write_data(uint8_t* p_data, size_t len)
{
    uint8_t control_byte = 0x40; //Co = 0, RS = 1
    auto* buffer = (uint8_t*)(malloc(len + 1)); //allocate memory for buffer
    buffer[0] = control_byte;
    memcpy(buffer + 1, p_data, len); //copy bytes from data to buffer
    i2c_transmit(buffer, len + 1);
    free(buffer);
    HAL_Delay(WRITE_DELAY);
}

/*
 * Transmit bytes to driver chip over I2C
 * p_data - pointer to data to transmit
 * len - number of bytes to transmit
 */
void Display::i2c_transmit(uint8_t* p_data, size_t len)
{
    if(HAL_I2C_Master_Transmit(m_p_hw_conf->p_hi2c, I2C_DEVICE_ADDR, p_data, len, I2C_TIMEOUT) != HAL_OK)
        Error_Handler();
}

