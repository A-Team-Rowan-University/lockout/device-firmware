/*
 * LockoutDevice.h
 *
 * Author: Andrew Hollabaugh
 * Date Created: 6/27/20
 */

#ifndef LOCKOUTDEVICE_H
#define LOCKOUTDEVICE_H

#include "interface.h"
#include "usart.h"
#include "Comms.h"
#include "Radio.h"
#include "RadioLowLevel.h"
#include "CardReader.h"
#include "Buzzer.h"
#include "Display.h"
#include "LCDMessages.h"
#include "EEPROM.h"

class LockoutDevice {
public:
	struct DeviceHWConf {
		RadioLowLevel::RadioHWConf radio_hw_conf;
		MFRC522::CardReaderHWConf card_reader_hw_conf;
		Display::DisplayHWConf display_hw_conf;
		Buzzer::BuzzerHWConf buzzer_hw_conf;
		EEPROM::EEPROMHwConf eeprom_hw_conf;
		UART_HandleTypeDef* p_debug_comms_huart;
		GPIO_TypeDef* LED_HB_GPIO_PORT;
		uint16_t LED_HB_GPIO_PIN;
		GPIO_TypeDef* LED_RX_GPIO_PORT;
		uint16_t LED_RX_GPIO_PIN;
		TIM_HandleTypeDef* p_rx_led_htim;
		GPIO_TypeDef* LED_TX_GPIO_PORT;
		uint16_t LED_TX_GPIO_PIN;
		TIM_HandleTypeDef* p_tx_led_htim;
		//GPIO_TypeDef* BTN_PING_GPIO_PORT;
		uint16_t BTN_PING_GPIO_PIN;
		//GPIO_TypeDef* BTN_EX_GPIO_PORT;
		//uint16_t BTN_EX_GPIO_PIN;
		GPIO_TypeDef* EXT_BTN0_GPIO_PORT;
		uint16_t EXT_BTN0_GPIO_PIN;
		GPIO_TypeDef* EXT_BTN1_GPIO_PORT;
		uint16_t EXT_BTN1_GPIO_PIN;
		GPIO_TypeDef* EXT_BTN2_GPIO_PORT;
		uint16_t EXT_BTN2_GPIO_PIN;
		GPIO_TypeDef* EXT_LED_GPIO_PORT;
		uint16_t EXT_LED_GPIO_PIN;
		GPIO_TypeDef* MCHN_EN_GPIO_PORT;
		uint16_t MCHN_EN_GPIO_PIN;
		GPIO_TypeDef* PB_FB_GPIO_PORT;
		uint16_t PB_FB_GPIO_PIN;
		GPIO_TypeDef* CARD_SW_GPIO_PORT;
		uint16_t CARD_SW_GPIO_PIN;
		GPIO_TypeDef* KEY_ENABLE_GPIO_PORT;
		uint16_t KEY_ENABLE_GPIO_PIN;
		GPIO_TypeDef* KEY_DISABLE_GPIO_PORT;
		uint16_t KEY_DISABLE_GPIO_PIN;
	};

	explicit LockoutDevice(DeviceHWConf* pmtr_p_device_hw_conf);

    [[noreturn]] void run();

    //functions triggered by interrupts in interface.cpp
	void on_rx_led_timer_overflow();
	void on_tx_led_timer_overflow();
	void on_radio_int();
	void on_ping_button_press();
	void on_note_timer_overflow();

	RadioLowLevel m_radio_low_level;

private:
	DeviceHWConf* m_p_device_hw_conf;

    Radio m_radio;
	CardReader m_card_reader;
    Display m_lcd;
    LCDMessages m_lcd_msgs;
    Buzzer m_buzzer;
    EEPROM m_eeprom;
    Comms m_db_comms;

    uint8_t m_my_addr = m_radio.UNASSIGNED_ADDR;
    uint8_t m_tag = 0;

	typedef enum {
		WAIT_FOR_RX,
		WAIT_FOR_TX,
	} radio_state_t;
	radio_state_t m_radio_state = WAIT_FOR_RX;

	enum {
		ACC_RESP_DENY = 0x00, //corresponds to 1st byte of data field for access response command
		ACC_RESP_ACCEPT = 0x01,
		ACC_RESP_ERROR = 0x02
	};
	typedef enum { //corresponds to 1st byte of data field for machine control and machine state commands
		MCHN_STATE_DISABLE = 0x00,
		MCHN_STATE_ENABLE = 0x01,
	} machine_state_t;
	machine_state_t m_machine_state = MCHN_STATE_DISABLE;

	bool m_admin_enabled = false; //enabled with keyswitch or web ui, only valid when m_machine_state is ENABLED

	//HEARTBEAT LED
	const uint32_t HB_HALF_PERIOD_TIME = 500; //time between led toggles
	uint32_t m_hb_last_toggle_time = 0;

	//PACKET TX RETRY
	const uint32_t RETRY_TIMEOUT = 5 * 1000;
	uint32_t m_last_retry_tx_time = 0; //the time value of when a retry was sent last
	bool m_wait_for_ack = false; //if waiting for ACK from a sent packet, if true for too long, goes into connection error mode
	uint8_t m_retry_count = 0; //number of times the same packet was sent and an ACK was not receieved
	Radio::payload_t m_last_tx_payload{}; //payload of packet sent previously

	//PINGS
	const uint32_t AUTO_PING_INTERVAL = 5 * 60 * 1000;
	uint32_t m_last_rx_time = 0; //the time value of when a packet was received last
	volatile bool m_ping_button = false; //is ping button pressed

	//PACKET RX DEDUPLICATION
	Radio::payload_t m_last_rx_payload{.tag=0xff}; //payload of packet received last, used for deduplication

	//CARD READING
	static const uint8_t NUM_CARD_READS = 10; //number of card reads compared by attempt_card_read
	const uint8_t NUM_CARD_READ_RETRIES = 10; //max retries that do_card_read_retries attempts
	bool m_card_inserted = false; //is card insert switch pressed (low), used for detecting signal edges
	uint8_t m_p_cur_id[16] = {0}; //current id card value, used for comparing with new reads
	bool is_authenticating = false;

	//CARD REMOVE COUNTDOWN
	const uint8_t CARD_REMOVE_TIMEOUT_SEC = 15; //countdown starting value
	uint32_t m_card_remove_time_from_last_sec = 0;
	uint8_t m_card_remove_countdown = 0; //current countdown value

	//REREAD CARD
	const uint32_t REREAD_CARD_TIME_INTERVAL = 10 * 1000; //card will be re-read every this amount of time
	uint32_t m_last_reread_card_time = 0;

	//EEPROM ID CACHE
	const uint8_t EEPROM_ID_CACHE_SIZE = 5; //number of IDs to store in cache (not number of bytes)

	//KEYSWITCH
	typedef enum {
		KEY_DEFAULT,
		KEY_ENABLE,
		KEY_DISABLE
	} keyswitch_pos_t;
	keyswitch_pos_t m_keyswitch_pos = KEY_DEFAULT;

	typedef enum {
		LOCK_MODE_OFF = 0x00,
		LOCK_MODE_ON_KEYSWITCH = 0x01,
		LOCK_MODE_ON_WEBUI = 0x02
	} lock_mode_t;
	lock_mode_t m_lock_mode = LOCK_MODE_OFF;

	//USER BUTTONS
	const uint32_t USER_BUTTON_PRESS_TIME = 50; //time user button must be held down
	typedef enum {
		USER_BUTTON_STATE_UNPRESSED,
		USER_BUTTON_STATE_PRESSED_NOT_EXPIRED,
		USER_BUTTON_STATE_PRESSED_EXPIRED,
	} user_button_state_t;
	typedef enum {
		USER_BUTTON_HELP,
		USER_BUTTON_REPORT,
		USER_BUTTON_BROKEN,
	} user_button_type_t;
	struct user_button_current_info {
		user_button_state_t state;
		user_button_type_t type;
		uint32_t press_time;
	};
	user_button_current_info m_button_info = {
			USER_BUTTON_STATE_UNPRESSED,
			USER_BUTTON_HELP, 0};

	bool m_ext_led_flashed = false;

	void tx_packet(Radio::cmd_t cmd, uint8_t (&data)[Radio::PAYLOAD_DATA_LEN], bool use_retry);
	void tx_ping();
	void tx_ack(Radio::cmd_t rx_cmd, uint8_t rx_tag, uint8_t rssi);
	void tx_addr_req();
	void tx_access_req(const uint8_t* p_id);
	void tx_machine_state(machine_state_t state);
	void tx_user_button(user_button_type_t button_type);
	void tx_lock_mode(lock_mode_t lock_mode);
	void on_rx_ping();
	void on_rx_addr_prov(uint8_t* p_rx_data);
	void on_rx_acc_resp(const uint8_t* p_rx_data);
	void on_rx_machine_control(const uint8_t* p_rx_data);
	void on_rx_set_lock_mode(const uint8_t* p_rx_data);
	void on_rx_clear_cache();
	void on_rx_ack(const uint8_t* p_rx_data);
	void set_machine_state(machine_state_t new_state);
	void set_lock_mode(lock_mode_t lock_mode);
	bool do_card_read_retries(uint8_t* p_id);
	bool attempt_card_read(uint8_t* p_result_id);
	static bool compare_ids(const uint8_t* p_id1, const uint8_t* p_id2);
	static void copy_ids(const uint8_t* p_src, uint8_t* p_dest);
	void write_eeprom_id_cache(uint8_t* p_id_to_write);
	bool check_eeprom_id_cache(uint8_t* p_id_to_check);
	void auth_using_id_cache(uint8_t* p_id);
	void disable_countdown_start(bool prev_message);
	void disable_countdown_update();
	void flash_rx_led();
	void flash_tx_led();
	void toggle_ext_led();
};

#endif //LOCKOUTDEVICE_H
