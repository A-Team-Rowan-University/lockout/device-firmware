/*
 * EEPROM.cpp
 *
 * Author: Andrew Hollabaugh
 * Date Created: 3/07/21
 *
 * Driver for AT24CS02 (probably any AT24 series IC would work) EEPROM IC
 */

#include <malloc.h>
#include "EEPROM.h"

EEPROM::EEPROM(EEPROMHwConf* pmtr_p_hw_conf)
{
	m_p_hw_conf = pmtr_p_hw_conf;
}

/*
 * Write a single byte to the specified address in EEPROM
 * addr - single byte address to write to
 * data - byte to write
 */
void EEPROM::write_byte(uint8_t addr, uint8_t data)
{
	uint8_t tx_data[2] = {0};
	tx_data[0] = addr;
	tx_data[1] = data;

	//disable write protect before sending data
	HAL_GPIO_WritePin(m_p_hw_conf->WP_GPIO_PORT, m_p_hw_conf->WP_GPIO_PIN, GPIO_PIN_RESET);
	i2c_tx(tx_data, 2);
	HAL_GPIO_WritePin(m_p_hw_conf->WP_GPIO_PORT, m_p_hw_conf->WP_GPIO_PIN, GPIO_PIN_SET);
}

/*
 * Write multiple bytes to EEPROM, address is incremented for every byte
 * addr - address of first byte to write
 * p_data - pointer to data to write
 * len - number of bytes to write
 */
void EEPROM::write_bytes(uint8_t addr, uint8_t* p_data, size_t len)
{
	for(size_t i = 0; i < len; i++)
	{
		write_byte(addr + i, p_data[i]);
	}
}

/*
 * Read a single byte from EEPROM
 * addr - address to read from
 * returns the byte that was read
 */
uint8_t EEPROM::read_byte(uint8_t addr)
{
	uint8_t data;
	i2c_tx(&addr, 1);
	i2c_rx(&data, 1);
	return data;
}

/*
 * Read multiple bytes from EEPROM, address is incremented for every byte read
 * addr - address of first byte to read
 * len - number of bytes to read
 * returns pointer to array of bytes read
 */
uint8_t* EEPROM::read_bytes(uint8_t addr, size_t len)
{
	auto* p_data = static_cast<uint8_t *>(malloc(len)); //allocate data to put read bytes in
	for(size_t i = 0; i < len; i++)
	{
		p_data[i] = read_byte(addr + i);
	}
	return p_data;
}

/*
 * Transmit bytes to EEPROM chip over I2C
 * p_data - pointer to first byte of data to transmit
 * len - number of bytes to transmit
 */
void EEPROM::i2c_tx(uint8_t* p_data, size_t len)
{
	HAL_Delay(1);
	if(HAL_I2C_Master_Transmit(m_p_hw_conf->p_hi2c, I2C_DEVICE_ADDR << 1, p_data, len, I2C_TIMEOUT) != HAL_OK)
		Error_Handler();
}

/*
 * Receive bytes from EEPROM chip over I2C
 * p_data - pointer to first byte of buffer to put received data in
 * len - number of bytes to receive
 */
void EEPROM::i2c_rx(uint8_t* p_data, size_t len)
{
	HAL_Delay(1);
	if(HAL_I2C_Master_Receive(m_p_hw_conf->p_hi2c, I2C_DEVICE_ADDR << 1, p_data, len, I2C_TIMEOUT) != HAL_OK)
		Error_Handler();
}
