/*
 * LockoutGateway.h
 *
 * Author: Andrew Hollabaugh
 * Date Created: 8/20/20
 */

#ifndef LOCKOUTGATEWAY_H
#define LOCKOUTGATEWAY_H

#include "interface.h"
#include "usart.h"
#include "Comms.h"
#include "Radio.h"
#include "RadioLowLevel.h"

class LockoutGateway {
public:
	struct GatewayHWConf {
		RadioLowLevel::RadioHWConf radio_hw_conf;
		//UART_HandleTypeDef* p_debug_comms_huart;
		UART_HandleTypeDef* p_rpi_comms_huart;
		GPIO_TypeDef* LED_HB_GPIO_PORT;
		uint16_t LED_HB_GPIO_PIN;
		GPIO_TypeDef* LED_RX_GPIO_PORT;
		uint16_t LED_RX_GPIO_PIN;
		TIM_HandleTypeDef* p_rx_led_htim;
		GPIO_TypeDef* LED_TX_GPIO_PORT;
		uint16_t LED_TX_GPIO_PIN;
		TIM_HandleTypeDef* p_tx_led_htim;
	};

	explicit LockoutGateway(GatewayHWConf* pmtr_p_gateway_hw_conf);

    [[noreturn]] void run();
    void on_rx_led_timer_overflow();
	void on_tx_led_timer_overflow();
    void on_radio_int();
    void on_rpi_uart_rx(uint8_t* buf);

	RadioLowLevel m_radio_low_level;

private:
	GatewayHWConf* m_p_gateway_hw_conf;

    Radio m_radio;
	//Comms m_db_comms;
	Comms m_rpi_comms;

	//HEARTBEAT LED
	const uint32_t HB_HALF_PERIOD_TIME = 500; //time between led toggles
	uint32_t m_hb_last_toggle_time = 0;

	//UART RX
	bool m_rpi_rx = false; //if in the middle of receiving a uart packet from raspberry pi
	size_t m_rpi_rx_num_bytes = 0; //number of bytes received so far from pi in packet
	static const size_t RPI_RX_LEN = Radio::PAYLOAD_LEN + 2; //+1 for dest-addr, +1 for rssi
	static const size_t RPI_RX_DATA_LEN = Radio::PAYLOAD_DATA_LEN;
	uint8_t m_rpi_rx_data[RPI_RX_LEN]{}; //buffer for data received over uart from pi

	typedef enum {
		WAIT_FOR_RX,
		WAIT_FOR_TX
	} radio_state_t;
	radio_state_t m_radio_state = WAIT_FOR_RX;

	void flash_rx_led();
	void flash_tx_led();
};

#endif //LOCKOUTGATEWAY_H
