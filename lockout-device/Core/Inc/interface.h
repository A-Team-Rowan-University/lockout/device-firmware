/*
 * interface.h
 *
 * Author: Andrew Hollabaugh
 * Date Created: 5/17/21
 *
 */

#ifndef INTERFACE_H
#define INTERFACE_H


#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "tim.h"

void lockout_device_main();

#ifdef __cplusplus
}
#endif

#endif //INTERFACE_H
