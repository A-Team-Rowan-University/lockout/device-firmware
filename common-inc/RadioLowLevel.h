/*
 * RadioLowLevel.h
 *
 * Author: Andrew Hollabaugh
 * Date Created: 8/6/20
 *
 * The 'glue' between the radio library (MCU_Interface.h) and STM32 hardware
 * Implements specific functions required by the radio library that access the STM32 hardware, see MCU_Interface.h in radio library
 *
 * The RadioLowLevel class only exists to get the hardware configuration from LockoutDevice or LockoutGateway,
 * all the functions do not belong to the class, though they access the hardware configuration from the class
 * This is a wierd way to do things, but the radio library does not use classes, and the functions it needs
 * to be implemented (MCU_Interface.h) cannot be in a class.
 *
 * A lot of things are copied and modified from various places in the SPIRIT1 X-CUBE-SUBG1 library example implementation
 */

#ifndef RADIO_LOW_LEVEL_H
#define RADIO_LOW_LEVEL_H

#include "spi.h"
#include "MCU_Interface.h"
#include "SPIRIT_Gpio.h"

#define POR_TIME ((uint16_t)0x1E00)
#define CS_TO_SCLK_DELAY     0x0100
#define CLK_TO_CS_DELAY      0x0100
#define HEADER_WRITE_MASK     0x00                                /*!< Write mask for header byte*/
#define HEADER_READ_MASK      0x01                                /*!< Read mask for header byte*/
#define HEADER_ADDRESS_MASK   0x00                                /*!< Address mask for header byte*/
#define HEADER_COMMAND_MASK   0x80                                /*!< Command mask for header byte*/

#define LINEAR_FIFO_ADDRESS   0xFF                                  /*!< Linear FIFO address*/

#define SPI_ENTER_CRITICAL()  RadioGpioInterruptCmd(RadioLowLevel::m_p_hw_conf->INT_IRQ_HANDLER,0x04,0x04,DISABLE)
#define SPI_EXIT_CRITICAL()   RadioGpioInterruptCmd(RadioLowLevel::m_p_hw_conf->INT_IRQ_HANDLER,0x04,0x04,ENABLE)
#define ALL_IRQ_ENABLE()  __enable_irq()
#define ALL_IRQ_DISABLE() __disable_irq()

#define RadioSpiCSLow()        HAL_GPIO_WritePin(RadioLowLevel::m_p_hw_conf->NSS_GPIO_PORT, RadioLowLevel::m_p_hw_conf->NSS_GPIO_PIN, GPIO_PIN_RESET)
#define RadioSpiCSHigh()       HAL_GPIO_WritePin(RadioLowLevel::m_p_hw_conf->NSS_GPIO_PORT, RadioLowLevel::m_p_hw_conf->NSS_GPIO_PIN, GPIO_PIN_SET)

#define BUILT_HEADER(add_comm, w_r) (add_comm | w_r)                             /*!< macro to build the header byte*/
#define WRITE_HEADER        BUILT_HEADER(HEADER_ADDRESS_MASK, HEADER_WRITE_MASK) /*!< macro to build the write
                                                                                                         header byte*/
#define READ_HEADER         BUILT_HEADER(HEADER_ADDRESS_MASK, HEADER_READ_MASK)  /*!< macro to build the read
                                                                                                         header byte*/
#define COMMAND_HEADER      BUILT_HEADER(HEADER_COMMAND_MASK, HEADER_WRITE_MASK) /*!< macro to build the command
                                                                                                         header byte*/

class RadioLowLevel {
public:
	struct RadioHWConf {
		SPI_HandleTypeDef* p_hspi;
		GPIO_TypeDef* NSS_GPIO_PORT;
		uint16_t NSS_GPIO_PIN;
		GPIO_TypeDef* SDN_GPIO_PORT;
		uint16_t SDN_GPIO_PIN;
		GPIO_TypeDef* INT_GPIO_PORT;
		uint16_t INT_GPIO_PIN;
		IRQn_Type INT_IRQ_HANDLER;
		SpiritGpioPin SPIRIT_INT_GPIO_PIN;
	};
	static RadioHWConf* m_p_hw_conf;
	const static uint16_t SPI_TIMEOUT = 1000;

    explicit RadioLowLevel(RadioHWConf* pmtr_p_hw_conf);
};

void RadioEnterShutdown();
void RadioExitShutdown();
SpiritFlagStatus RadioCheckShutdown();
void RadioGpioInterruptCmd(IRQn_Type IRQ_HANDLER, uint8_t nPreemption, uint8_t nSubpriority, FunctionalState xNewState);
static void SPI_Write(uint8_t Value);
static void SPI_Error();

#endif //RADIO_LOW_LEVEL_H
