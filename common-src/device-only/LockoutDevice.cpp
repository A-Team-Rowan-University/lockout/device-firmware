/*
 * LockoutDevice.cpp
 *
 * Author: Andrew Hollabaugh
 * Date Created: 6/27/20
 *
 * Application code for lockout devices - contains main loop
 */

#include <cstring>
#include "LockoutDevice.h"

LockoutDevice::LockoutDevice(DeviceHWConf* pmtr_p_device_hw_conf):
	m_radio_low_level(&pmtr_p_device_hw_conf->radio_hw_conf),
	m_radio(&m_db_comms),
	m_card_reader(&pmtr_p_device_hw_conf->card_reader_hw_conf, &m_db_comms),
	m_lcd(&pmtr_p_device_hw_conf->display_hw_conf),
	m_lcd_msgs(&m_lcd, &m_buzzer),
	m_buzzer(&pmtr_p_device_hw_conf->buzzer_hw_conf),
	m_eeprom(&pmtr_p_device_hw_conf->eeprom_hw_conf),
	m_db_comms(pmtr_p_device_hw_conf->p_debug_comms_huart)
{
	m_p_device_hw_conf = pmtr_p_device_hw_conf;
}

[[noreturn]] void LockoutDevice::run()
{
	//start led timers
	HAL_TIM_Base_Start_IT(m_p_device_hw_conf->p_rx_led_htim);
	HAL_TIM_Base_Start_IT(m_p_device_hw_conf->p_tx_led_htim);

	m_radio.init();
	m_radio.set_addr(m_my_addr);
	m_radio.start_rx(0);
	m_card_reader.init();
	m_lcd.init();
	m_lcd_msgs.init_buzzer();
	m_lcd_msgs.display_msg(LCDMessages::LCD_MSG_INIT);
	tx_addr_req();
	m_buzzer.play_beep(m_buzzer.startup);

	while(true)
    {
	    m_lcd_msgs.update_timeout();

		//check if it is time for the heartbeat led to be toggled
		if(HAL_GetTick() - m_hb_last_toggle_time > HB_HALF_PERIOD_TIME)
		{
			HAL_GPIO_TogglePin(m_p_device_hw_conf->LED_HB_GPIO_PORT, m_p_device_hw_conf->LED_HB_GPIO_PIN);
			m_hb_last_toggle_time = HAL_GetTick();
		}

	    //code to handle keyswitch enable mode
	    if(HAL_GPIO_ReadPin(m_p_device_hw_conf->KEY_ENABLE_GPIO_PORT, \
				m_p_device_hw_conf->KEY_ENABLE_GPIO_PIN) == GPIO_PIN_SET) //keyswitch in enable position (enable signal is high))
	    {
		    if(m_keyswitch_pos == KEY_DEFAULT) //only on rising edge -> set to enable mode
		    {
			    m_keyswitch_pos = KEY_ENABLE;
			    set_machine_state(MCHN_STATE_ENABLE);
			    m_admin_enabled = true;
			    m_card_remove_countdown = 0;
			    m_lcd_msgs.display_msg(LCDMessages::LCD_MSG_ADMIN_ENABLE);
			    if(!m_wait_for_ack)
			    {
				    tx_machine_state(MCHN_STATE_ENABLE);
			    }
		    }
	    }
	    else
	    {
		    if(m_keyswitch_pos == KEY_ENABLE) //only on falling edge -> set to default mode
		    {
			    m_keyswitch_pos = KEY_DEFAULT;
			    set_machine_state(MCHN_STATE_DISABLE);
			    m_card_remove_countdown = 0;
			    m_lcd_msgs.display_msg(m_lcd_msgs.m_idle_msg_name);
			    if(!m_wait_for_ack)
			    {
				    tx_machine_state(MCHN_STATE_DISABLE);
			    }
		    }
	    }

	    //code to handle keyswitch handling of lock mode
	    if(HAL_GPIO_ReadPin(m_p_device_hw_conf->KEY_DISABLE_GPIO_PORT, \
				m_p_device_hw_conf->KEY_DISABLE_GPIO_PIN) == GPIO_PIN_SET) //keyswitch in disable position (disable signal is high)
	    {
		    if(m_keyswitch_pos == KEY_DEFAULT) //only on rising edge -> set lock mode
		    {
			    m_keyswitch_pos = KEY_DISABLE;
			    set_lock_mode(LOCK_MODE_ON_KEYSWITCH);
			    if(!m_wait_for_ack)
			    {
				    tx_lock_mode(LOCK_MODE_ON_KEYSWITCH);
			    }
		    }
	    }
	    else
	    {
		    if(m_keyswitch_pos == KEY_DISABLE) //only on falling edge -> set to default mode
		    {
			    m_keyswitch_pos = KEY_DEFAULT;
			    set_lock_mode(LOCK_MODE_OFF);
			    if(!m_wait_for_ack)
			    {
				    tx_lock_mode(LOCK_MODE_OFF);
			    }
		    }
	    }

		if(HAL_GPIO_ReadPin(m_p_device_hw_conf->CARD_SW_GPIO_PORT, \
				m_p_device_hw_conf->CARD_SW_GPIO_PIN) == GPIO_PIN_RESET) //card switch is pressed
		{
			if(!m_card_inserted && //ensure only falling edge of card switch signal
				(m_machine_state == MCHN_STATE_DISABLE || m_card_remove_countdown > 0) && //not already enabled, or during card remove countdown
				m_lock_mode == LOCK_MODE_OFF)
			{
				m_card_inserted = true;
				if (HAL_GPIO_ReadPin(m_p_device_hw_conf->PB_FB_GPIO_PORT, \
						m_p_device_hw_conf->PB_FB_GPIO_PIN) == GPIO_PIN_SET) //only if power board feedback signal is high (default)
				{
					m_lcd_msgs.display_msg(LCDMessages::LCD_MSG_AUTH);
					uint8_t p_id[16] = {0};
					bool success = do_card_read_retries(p_id); //does all the card reading with retries and stuff
					if(success) //if card was read properly
					{
						m_card_reader.print_id(p_id); //print id to serial port

						//for if machine was previously enabled, card removed and reinserted before countdown expires
						//if ensure card number is same as previous stored id number (m_p_cur_id), enable without authenticating with server
						if(m_card_remove_countdown > 0 && compare_ids(p_id, m_p_cur_id))
						{
							m_lcd_msgs.display_msg(LCDMessages::LCD_MSG_MCHN_ENABLE);
							set_machine_state(MCHN_STATE_ENABLE);
						}
						else
						{
							if(!m_wait_for_ack) //communication is working
							{
								is_authenticating = true;
								tx_access_req(p_id); //send authentication request to server
							}
							else
							{
								auth_using_id_cache(p_id);
							}
						}
						copy_ids(p_id, m_p_cur_id); //set m_p_cur_id to the new id that was read for later
						m_card_remove_countdown = 0; //disable card remove countdown, to not cause problems if card is re-inserted during countdown
					}
					else
					{
						m_lcd_msgs.display_msg(LCDMessages::LCD_MSG_CARD_READ_ERROR);
						if(m_card_remove_countdown > 0)
						{
							m_lcd_msgs.m_current_default_msg_name = LCDMessages::LCD_MSG_MCHN_DISABLE;
						}
					}
				}
				else
				{
					m_lcd_msgs.display_msg(LCDMessages::LCD_MSG_PB_FB);
					if(m_card_remove_countdown > 0)
					{
						m_lcd_msgs.m_current_default_msg_name = LCDMessages::LCD_MSG_MCHN_DISABLE;
					}
				}
			}
		}
		else //card switch is unpressed - handle countdown for machine disable
		{
			//only on rising edge of switch signal and machine is enabled
			if(m_card_inserted)
			{
				m_card_inserted = false;

				//if enabled, start the disable countdown if not already started
				if(m_machine_state == MCHN_STATE_ENABLE && !m_admin_enabled && m_card_remove_countdown == 0)
				{
					disable_countdown_start(false);
				}
			}
		}

	    disable_countdown_update();

	    //re-read card every REREAD_CARD_TIME_INTERVAL milliseconds to ensure someone didn't sneakily switch the cards
	    if(m_machine_state == MCHN_STATE_ENABLE && \
	            !m_admin_enabled && m_card_remove_countdown == 0 && \
				HAL_GetTick() - m_last_reread_card_time > REREAD_CARD_TIME_INTERVAL)
	    {
		    uint8_t p_id[16] = {0};
		    bool success = do_card_read_retries(p_id);
		    if(!success || !compare_ids(p_id, m_p_cur_id)) //either card did not read or id is different from before -> disable
		    {
			    m_lcd_msgs.display_msg(LCDMessages::LCD_MSG_CARD_CHECK_ERROR);
				disable_countdown_start(true);
		    }
		    m_last_reread_card_time = HAL_GetTick();
	    }

	    if(m_wait_for_ack)
	    {
		    //if retry timeout expires, resend message and display connection error
		    if(HAL_GetTick() - m_last_retry_tx_time > RETRY_TIMEOUT)
		    {
			    m_db_comms.write_str("tx retry\n");

				if(m_my_addr == m_radio.UNASSIGNED_ADDR)
			    {
					tx_addr_req();
				}
				else if(m_last_tx_payload.cmd == m_radio.CMD_ACC_REQ)
				{
					tx_ping();
				}
				else
			    {
					tx_packet(m_last_tx_payload.cmd, m_last_tx_payload.data, true);
				}

			    if(m_lcd_msgs.m_current_default_msg_name == LCDMessages::LCD_MSG_NULL ||
				    m_lcd_msgs.m_current_default_msg_name == LCDMessages::LCD_MSG_IDLE)
			    {
				    m_lcd_msgs.display_msg(LCDMessages::LCD_MSG_CONNECTION_ERROR);
			    }

				if(is_authenticating)
				{
					auth_using_id_cache(m_p_cur_id);
					is_authenticating = false;
				}
		    }
	    }
	    else
	    {
			//send a ping if ping button pressed or timeout for automatic pinging expires
			if(m_ping_button || (HAL_GetTick() - m_last_rx_time > AUTO_PING_INTERVAL))
			{
				m_ping_button = false;
				tx_ping();
			}

			//code to handle user buttons (help, report, broken)
			bool any_user_button_low = false;
			if(HAL_GPIO_ReadPin(m_p_device_hw_conf->EXT_BTN0_GPIO_PORT, \
				m_p_device_hw_conf->EXT_BTN0_GPIO_PIN) == GPIO_PIN_RESET) //help button pressed
			{
				m_button_info.type = USER_BUTTON_HELP;
				any_user_button_low = true;
			}
			if(HAL_GPIO_ReadPin(m_p_device_hw_conf->EXT_BTN1_GPIO_PORT, \
				m_p_device_hw_conf->EXT_BTN1_GPIO_PIN) == GPIO_PIN_RESET) //report button pressed
			{
				m_button_info.type = USER_BUTTON_REPORT;
				any_user_button_low = true;
			}
			if(HAL_GPIO_ReadPin(m_p_device_hw_conf->EXT_BTN2_GPIO_PORT, \
				m_p_device_hw_conf->EXT_BTN2_GPIO_PIN) == GPIO_PIN_RESET) //broken button pressed
			{
				m_button_info.type = USER_BUTTON_BROKEN;
				any_user_button_low = true;
			}
			if(any_user_button_low)
			{
				switch(m_button_info.state)
				{
					case USER_BUTTON_STATE_UNPRESSED:
						m_button_info.state = USER_BUTTON_STATE_PRESSED_NOT_EXPIRED;
						break;
					case USER_BUTTON_STATE_PRESSED_NOT_EXPIRED:
						//button must be pressed down for USER_BUTTON_PRES_TIME milliseconds
						if(HAL_GetTick() - m_button_info.press_time > USER_BUTTON_PRESS_TIME)
						{
							tx_user_button(m_button_info.type);
							switch(m_button_info.type)
							{
								case USER_BUTTON_HELP:
									m_lcd_msgs.display_msg(LCDMessages::LCD_MSG_HELP_BTN);
									break;
								case USER_BUTTON_REPORT:
									m_lcd_msgs.display_msg(LCDMessages::LCD_MSG_REPORT_BTN);
									break;
								case USER_BUTTON_BROKEN:
									m_lcd_msgs.display_msg(LCDMessages::LCD_MSG_BROKEN_BTN);
									break;
							}
							m_button_info.state = USER_BUTTON_STATE_PRESSED_EXPIRED;
						}
						break;
					case USER_BUTTON_STATE_PRESSED_EXPIRED:
						break;
				}
			}
			else
			{
				m_button_info.state = USER_BUTTON_STATE_UNPRESSED;
			}
		}

	    switch(m_radio_state)
	    {
	    	case WAIT_FOR_RX: //radio in rx mode
				if(m_radio.rx_done) //radio received a packet
				{
					m_radio.rx_done = false;
					flash_rx_led();
					Radio::payload_t rx_payload{};
					m_db_comms.write_str("==RX PACKET START==\n"); //get_rx_payload prints payload to serial, put header and footer around it
					m_radio.get_rx_payload(&rx_payload); //get the packet payload from the radio
					m_db_comms.write_str("\n==RX PACKET END==\n");
					uint8_t rssi = SpiritQiGetRssi();

					//ignore packet if tag is same as last time -> duplicate packet
					if(rx_payload.tag != m_last_rx_payload.tag)
					{
						m_last_rx_time = HAL_GetTick();
						switch (rx_payload.cmd)
						{
							case Radio::CMD_PING:
								on_rx_ping();
								tx_ack(rx_payload.cmd, rx_payload.tag, rssi);
								break;
							case Radio::CMD_ADDR_PROV:
								on_rx_addr_prov(rx_payload.data);
								break;
							case Radio::CMD_ACC_RESP:
								on_rx_acc_resp(rx_payload.data);
								break;
							case Radio::CMD_MCHN_CTRL:
								on_rx_machine_control(rx_payload.data);
								tx_ack(rx_payload.cmd, rx_payload.tag, rssi);
								break;
							case Radio::CMD_LOCK_MODE:
								on_rx_set_lock_mode(rx_payload.data);
								tx_ack(rx_payload.cmd, rx_payload.tag, rssi);
								break;
							case Radio::CMD_CLEAR_CACHE:
								on_rx_clear_cache();
								tx_ack(rx_payload.cmd, rx_payload.tag, rssi);
								break;
							case Radio::CMD_ACK:
								on_rx_ack(rx_payload.data);
								break;
							default:
								break;
						}

						m_last_rx_payload = rx_payload;
					}
					else
					{
						m_db_comms.write_str("rx duplicate discarded\n");
					}
				}
				break;
			case WAIT_FOR_TX: //radio in tx mode - waiting for itself to transmit the packet, very short time
				if(m_radio.tx_done) //radio just finished transmit
				{
					m_radio.tx_done = false;
					flash_tx_led();
					m_radio.start_rx(0); //go back into rx mode
					m_radio_state = WAIT_FOR_RX;
				}
				break;
	    }
    }
}

/*
 * Transmit a packet - wrapper to include things that are the same for LockoutDevice
 * use_retry - enable the retry system: if not receive ack, put device in connection error mode and retry sending last
 * packet in a loop, not used for sending acks
 */
void LockoutDevice::tx_packet(Radio::cmd_t cmd, uint8_t (&data)[Radio::PAYLOAD_DATA_LEN], bool use_retry)
{
	Radio::payload_t tx_payload{};
	tx_payload.cmd = cmd;
	tx_payload.source_addr = m_my_addr;
	tx_payload.tag = m_tag++;
	tx_payload.retry = m_retry_count;
	for(uint8_t i = 0; i < Radio::PAYLOAD_DATA_LEN; i++)
	{
		tx_payload.data[i] = data[i];
	}

	m_db_comms.write_str("==TX PACKET START==\n"); //start_tx prints stuff to serial, so put header/footer around it
	m_radio.start_tx(m_radio.GATEWAY_ADDR, &tx_payload); //actually transmit the packet
	m_db_comms.write_str("\n==TX PACKET END==\n");

	if(use_retry)
	{
		m_last_tx_payload = tx_payload;
		m_retry_count++;
		m_wait_for_ack = true;
		m_last_retry_tx_time = HAL_GetTick();
	}
	m_radio_state = WAIT_FOR_TX;
}

/*
 * Transmit a ping, data field blank
 */
void LockoutDevice::tx_ping()
{
	m_db_comms.write_str("tx ping\n");
	uint8_t data[Radio::PAYLOAD_DATA_LEN] = {0};
	tx_packet(Radio::CMD_PING, data, true);
}

/*
 * Transmit an ACK, data field includes command of last received packet
 */
void LockoutDevice::tx_ack(Radio::cmd_t rx_cmd, uint8_t rx_tag, uint8_t rssi)
{
	m_db_comms.write_str("tx ack\n");
	uint8_t data[Radio::PAYLOAD_DATA_LEN] = {0};
	data[0] = rx_cmd;
	data[1] = rx_tag;
	data[2] = rssi;
	HAL_Delay(5); //a little delay so gateway is ready
	tx_packet(Radio::CMD_ACK, data, false);
}

/*
 * Transmit an address request - gets internal unique ID and copies to data field of packet
 */
void LockoutDevice::tx_addr_req()
{
	uint8_t data[Radio::PAYLOAD_DATA_LEN] = {0};

	uint32_t uid2 = HAL_GetUIDw2(); //each is 4 bytes totalling 12 bytes
	uint32_t uid1 = HAL_GetUIDw1();
	uint32_t uid0 = HAL_GetUIDw0();

	memcpy(&data[0], &uid2, 4); //copy uid data to data field, 4 bytes at a time
	memcpy(&data[4], &uid1, 4);
	memcpy(&data[8], &uid0, 4);

	for (uint8_t i = 12; i < Radio::PAYLOAD_DATA_LEN; i++) //set last 4 bytes of data field to 0
	{
		data[i] = 0;
	}

	m_db_comms.write_str("tx addr req\n");
	tx_packet(Radio::CMD_ADDR_REQ, data, true);
}

/*
 * Transmit access request - data field contains 16 byte id
 */
void LockoutDevice::tx_access_req(const uint8_t* p_id)
{
	m_db_comms.write_str("tx access req\n");
	uint8_t data[Radio::PAYLOAD_DATA_LEN] = {0};
	copy_ids(p_id, data); //copy id data from p_id to data field
	tx_packet(Radio::CMD_ACC_REQ, data, true);
}

/*
 * Transmit machine state - data field contains values of machine_state_t
 * Used to update server when machine state changes, but only when server is not already aware, like for override switch
 */
void LockoutDevice::tx_machine_state(machine_state_t machine_state)
{
	m_db_comms.write_str("tx machine state\n");
	uint8_t data[Radio::PAYLOAD_DATA_LEN] = {0};
	data[0] = machine_state;
	tx_packet(Radio::CMD_MCHN_STATE, data, true);
}

/*
 * Transmit user button command - data field contains values of user_button_type_t, which user button is pressed
 */
void LockoutDevice::tx_user_button(LockoutDevice::user_button_type_t button_type)
{
	m_db_comms.write_str("tx user button\n");
	uint8_t data[Radio::PAYLOAD_DATA_LEN] = {0};
	data[0] = (uint8_t)(button_type);
	tx_packet(Radio::CMD_USER_BUTTON, data, true);
}

void LockoutDevice::tx_lock_mode(LockoutDevice::lock_mode_t lock_mode)
{
	m_db_comms.write_str("tx lock mode\n");
	uint8_t data[Radio::PAYLOAD_DATA_LEN] = {0};
	data[0] = lock_mode;
	tx_packet(Radio::CMD_LOCK_MODE, data, true);
}

/*
 * Run when a ping is receieved
 */
void LockoutDevice::on_rx_ping()
{
	m_db_comms.write_str("rx ping\n");
}

/*
 * Run when an address provide command is received - copy unique id from data field and compare to internal unique id
 * Used in place of an ACK
 */
void LockoutDevice::on_rx_addr_prov(uint8_t* p_rx_data)
{
	uint32_t uid_rx2 = 0;
	uint32_t uid_rx1 = 0;
	uint32_t uid_rx0 = 0;

	memcpy(&uid_rx2, &p_rx_data[0], 4); //copy data field from packet to uid_rx variables, 4 bytes at a time to put in uint32_t format
	memcpy(&uid_rx1, &p_rx_data[4], 4);
	memcpy(&uid_rx0, &p_rx_data[8], 4);

	if(uid_rx2 == HAL_GetUIDw2() && \
		uid_rx1 == HAL_GetUIDw1() && \
		uid_rx0 == HAL_GetUIDw0()) //compare 3 pieces of the unique id between what is in this chip, and what is received from packet
	{
		m_my_addr = p_rx_data[12]; //if all bytes match, accept address from byte 12 of data field
		m_radio.set_addr(m_my_addr); //set address in radio IC register
		m_db_comms.write_str("rx addr prov: accept\n");
		m_lcd_msgs.display_msg(m_lcd_msgs.m_idle_msg_name);
	}
	else
	{
		m_db_comms.write_str("rx addr prov: reject\n");
	}

	m_retry_count = 0; //reset retry status vars, since no ack is required
	m_wait_for_ack = false;
}

/*
 * Run when an access response command is received
 * Enable machine if get an accept, always updates lcd
 * Used in place of an ACK
 */
void LockoutDevice::on_rx_acc_resp(const uint8_t* p_rx_data)
{
	is_authenticating = false;
	if(m_card_inserted)
	{
		switch(p_rx_data[0]) //first byte of data field contains the server's response code
		{
			case ACC_RESP_DENY:
				m_lcd_msgs.display_msg(LCDMessages::LCD_MSG_ACC_RESP_DENY);
				break;
			case ACC_RESP_ACCEPT:
				m_lcd_msgs.display_msg(LCDMessages::LCD_MSG_MCHN_ENABLE);
				set_machine_state(MCHN_STATE_ENABLE);
				m_admin_enabled = false;
				write_eeprom_id_cache(m_p_cur_id);
				break;
			case ACC_RESP_ERROR: //for if machine isn't set up properly in server
				m_lcd_msgs.display_msg(LCDMessages::LCD_MSG_ACC_RESP_ERROR);
				break;
		}

		m_retry_count = 0; //reset retry status vars, since no ack is required
		m_wait_for_ack = false;
	}
	else //card was removed while authenticating
	{
		m_lcd_msgs.display_msg(m_lcd_msgs.m_idle_msg_name);
		tx_machine_state(MCHN_STATE_DISABLE);
	}
}

/*
 * Run when a machine control command is received
 * Set new machine state based on first byte of data field
 */
void LockoutDevice::on_rx_machine_control(const uint8_t* p_rx_data)
{
	m_db_comms.write_str("rx machine control\n");
	switch(p_rx_data[0]) //first byte of data field determines new state for machine
	{
		case MCHN_STATE_DISABLE:
			if(m_keyswitch_pos != KEY_ENABLE)
			{
				m_lcd_msgs.display_msg(m_lcd_msgs.m_idle_msg_name);
				set_machine_state(MCHN_STATE_DISABLE);
				m_card_remove_countdown = 0;
			}
			break;
		case MCHN_STATE_ENABLE:
			if(m_lock_mode == LOCK_MODE_OFF)
			{
				m_lcd_msgs.display_msg(LCDMessages::LCD_MSG_ADMIN_ENABLE);
				set_machine_state(MCHN_STATE_ENABLE);
				m_admin_enabled = true;
				m_card_remove_countdown = 0;
			}
			break;
	}
}

/*
 * Run when a set lock mode command is received
 */
void LockoutDevice::on_rx_set_lock_mode(const uint8_t *p_rx_data)
{
	m_db_comms.write_str("rx set lock mode");
	switch(p_rx_data[0])
	{
		case 0x00:
			if(m_lock_mode != LOCK_MODE_ON_KEYSWITCH) //keyswitch has priority
			{
				set_lock_mode(LOCK_MODE_OFF);
			}
			break;
		case 0x01:
			set_lock_mode(LOCK_MODE_ON_WEBUI);
			break;
	}
}

/*
 * Run when a clear cache command is received
 * Clears the EEPROM ID cache to all 0xff
 */
void LockoutDevice::on_rx_clear_cache()
{
	m_db_comms.write_str("rx clear cache\n");
	uint8_t clear_value = 0xff;
	m_eeprom.write_bytes(1, &clear_value, EEPROM_ID_CACHE_SIZE * 16);
}

/*
 * Run when an ACK is received
 * Reset retry status
 */
void LockoutDevice::on_rx_ack(const uint8_t* p_rx_data)
{
	m_db_comms.write_str("rx ack\n");

	//ensure data and tag fields match the last sent packet - ensure this ack is actually for that packet
	if(p_rx_data[0] == m_last_tx_payload.cmd && p_rx_data[1] == m_last_tx_payload.tag)
	{
		m_retry_count = 0; //reset retry status vars
		m_wait_for_ack = false;

		//if current displayed message is connection error, display idle
		if(m_lcd_msgs.m_current_msg_name == m_lcd_msgs.LCD_MSG_CONNECTION_ERROR)
		{
			m_lcd_msgs.display_msg(m_lcd_msgs.m_idle_msg_name);
		}
	}
}

/*
 * Set the state of the machine to enable/disable
 */
void LockoutDevice::set_machine_state(machine_state_t new_state)
{
	switch(new_state)
	{
		case MCHN_STATE_DISABLE: //turn off external led and machine enable signal
			if(m_machine_state == MCHN_STATE_ENABLE)
			{
				HAL_GPIO_WritePin(m_p_device_hw_conf->EXT_LED_GPIO_PORT, m_p_device_hw_conf->EXT_LED_GPIO_PIN, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(m_p_device_hw_conf->MCHN_EN_GPIO_PORT, m_p_device_hw_conf->MCHN_EN_GPIO_PIN, GPIO_PIN_RESET);
				m_buzzer.play_beep(m_buzzer.disable);
			}
			break;
		case MCHN_STATE_ENABLE: //turn on external led and machine enable signal
			if(m_machine_state == MCHN_STATE_DISABLE)
			{
				HAL_GPIO_WritePin(m_p_device_hw_conf->EXT_LED_GPIO_PORT, m_p_device_hw_conf->EXT_LED_GPIO_PIN, GPIO_PIN_SET);
				HAL_GPIO_WritePin(m_p_device_hw_conf->MCHN_EN_GPIO_PORT, m_p_device_hw_conf->MCHN_EN_GPIO_PIN, GPIO_PIN_SET);
				m_buzzer.play_beep(m_buzzer.enable);
			}
			break;
	}
	m_machine_state = new_state;
}

void LockoutDevice::set_lock_mode(LockoutDevice::lock_mode_t lock_mode)
{
	if(lock_mode == LOCK_MODE_OFF)
	{
		m_lcd_msgs.m_idle_msg_name = LCDMessages::LCD_MSG_IDLE;
		if(m_lcd_msgs.m_current_msg_name == LCDMessages::LCD_MSG_LOCK_MODE)
		{
			m_lcd_msgs.display_msg(LCDMessages::LCD_MSG_IDLE);
		}
	}
	else //LOCK_MODE_ON_KEYSWITCH or LOCK_MODE_ON_WEBUI
	{
		m_lcd_msgs.m_idle_msg_name = LCDMessages::LCD_MSG_LOCK_MODE;
		if(m_lcd_msgs.m_current_msg_name == LCDMessages::LCD_MSG_IDLE ||
			m_lcd_msgs.m_current_msg_name == LCDMessages::LCD_MSG_CONNECTION_ERROR)
		{
			m_lcd_msgs.display_msg(LCDMessages::LCD_MSG_LOCK_MODE);
		}
	}
	m_lock_mode = lock_mode;
}

/*
 * Handle the card read operation - if mutli-read operation fails, try again
 * p_id - pointer to id that is read (result)
 */
bool LockoutDevice::do_card_read_retries(uint8_t* p_id)
{
	uint8_t card_read_retry = 0;
	bool success = false;
	while(card_read_retry < NUM_CARD_READ_RETRIES) //keep retrying
	{
		if(attempt_card_read(p_id)) //do a multi-read operation, check if it succeeds
		{
			success = true; //it succeeded, we are done
			break;
		}
		else
		{
			card_read_retry++; //try again
		}
	}
	return success;
}

/*
 * Read the id card several times, and check to make sure the value read is the same for all
 * p_result_id - pointer to id value that results from read operation - invalid if returns false
 * returns if card read was successful
 * First id read is stored, and all subsequent ids read and compared to first. If any are different, it fails
 */
bool LockoutDevice::attempt_card_read(uint8_t* p_result_id)
{
	bool success = true;
	for(uint8_t i = 0; i < NUM_CARD_READS; i++)
	{
		uint8_t* p_id = m_card_reader.check_id(); //read id once
		if(p_id == nullptr) //could not read card
		{
			success = false;
			break; //single read failed (probably card not in range) - exit loop
		}
		if(i == 0) //first read was successful - copy id into p_result_id
		{
			copy_ids(p_id, p_result_id);
		}
		else if(!compare_ids(p_id, p_result_id)) //not first read - check if newly read number is same as first read number
		{
			success = false; //id numbers were not the same - fail and exit loop
			break;
		}
	}
	return success;
}

/*
 * Compare two id numbers
 * p_id1, p_id2 - pointers to the two id numbers
 * returns true if ids are same, false if different
 */
bool LockoutDevice::compare_ids(const uint8_t* p_id1, const uint8_t* p_id2)
{
	bool same = true;
	for(uint8_t i = 0; i < 16; i++)
	{
		if(p_id1[i] != p_id2[i])
		{
			same = false;
			break;
		}
	}
	return same;
}

/*
 * Copy an id number to a new memory location
 * p_src - pointer to id to copy
 * p_dest - pointer to place to copy id to
 */
void LockoutDevice::copy_ids(const uint8_t* p_src, uint8_t* p_dest)
{
	for(uint8_t i = 0; i < 16; i++)
	{
		p_dest[i] = p_src[i];
	}
}

/*
 * How EEPROM cache works:
 * Address 0 contains the address of the most recent id in the cache (cache_ptr)
 * Addresses 1 to (EEPROM_ID_CACHE_SIZE * 16) + 1 contain the ID values
 * When an ID is written to the cache, it is first checked to see if it is already there to prevent duplicates (cache only contains unique IDs)
 * The cache_ptr is updated before each write, then the ID is written cache_ptr address
 */

/*
 * Check if the provided ID number is within the EEPROM cache
 * p_id_to_check - pointer to 16 byte ID to check against the cache
 */
bool LockoutDevice::check_eeprom_id_cache(uint8_t *p_id_to_check)
{
	bool is_id_in_cache = false;
	for(uint8_t cache_ptr = 0; cache_ptr < EEPROM_ID_CACHE_SIZE; cache_ptr++)
	{
		uint8_t* p_id_in_cache = m_eeprom.read_bytes(cache_ptr * 16 + 1, 16);
		if(compare_ids(p_id_to_check, p_id_in_cache))
		{
			is_id_in_cache = true;
			break;
		}
	}
	return is_id_in_cache;
}

/*
 * Write an ID number to the EEPROM cache
 * p_id_to_write - pointer to the 16 byte ID value to write
 */
void LockoutDevice::write_eeprom_id_cache(uint8_t *p_id_to_write)
{
	if(!check_eeprom_id_cache(p_id_to_write))
	{
		uint8_t cache_ptr = m_eeprom.read_byte(0);
		if(cache_ptr > EEPROM_ID_CACHE_SIZE) //will also be true if cache was just cleared (0xff)
		{
			cache_ptr = 0;
		}
		else
		{
			cache_ptr++;
		}
		m_eeprom.write_bytes(cache_ptr * 16 + 1, p_id_to_write, 16);
		m_eeprom.write_byte(0, cache_ptr);
	}
}

void LockoutDevice::auth_using_id_cache(uint8_t *p_id)
{
	if(m_card_inserted)
	{
		if(check_eeprom_id_cache(p_id))
		{
			m_lcd_msgs.display_msg(LCDMessages::LCD_MSG_MCHN_ENABLE);
			set_machine_state(MCHN_STATE_ENABLE);
		}
		else
		{
			m_lcd_msgs.display_msg(LCDMessages::LCD_MSG_ACC_RESP_DENY);
		}
	}
}

void LockoutDevice::disable_countdown_start(bool prev_message)
{
	m_lcd_msgs.m_current_default_msg_name = LCDMessages::LCD_MSG_MCHN_DISABLE;
	if(!prev_message)
	{
		m_lcd_msgs.display_msg(LCDMessages::LCD_MSG_MCHN_DISABLE);
		m_lcd.write_str("         " + std::to_string(CARD_REMOVE_TIMEOUT_SEC), \
					Display::LT_BOTTOM); //display max countdown value
	}
	m_card_remove_time_from_last_sec = HAL_GetTick();
	m_card_remove_countdown = CARD_REMOVE_TIMEOUT_SEC;
}

void LockoutDevice::disable_countdown_update()
{
	//whenever card switch is unpressed and countdown is active, check if a second has elapsed
	if(m_card_remove_countdown > 0 && HAL_GetTick() - m_card_remove_time_from_last_sec > 1000)
	{
		if(m_lcd_msgs.m_current_msg_name == LCDMessages::LCD_MSG_MCHN_DISABLE)
		{
			m_lcd.write_str("         " + std::to_string(m_card_remove_countdown - 1) + \
                            "          ", Display::LT_BOTTOM); //display next countdown value
		}
		m_card_remove_time_from_last_sec = HAL_GetTick();
		m_card_remove_countdown--;

		if(m_card_remove_countdown == 0) //disable machine when countdown goes to 0
		{
			set_machine_state(MCHN_STATE_DISABLE);
			tx_machine_state(MCHN_STATE_DISABLE);
			m_lcd_msgs.display_msg(m_lcd_msgs.m_idle_msg_name);
		}
	}
}

/*
 * Turn on RX led and set a timer to turn it off shortly
 */
void LockoutDevice::flash_rx_led()
{
	HAL_GPIO_WritePin(m_p_device_hw_conf->LED_RX_GPIO_PORT,
	                  m_p_device_hw_conf->LED_RX_GPIO_PIN, GPIO_PIN_SET);
	reset_timer(m_p_device_hw_conf->p_rx_led_htim);
}

/*
 * Turn on TX led and set a timer to turn it off shortly
 */
void LockoutDevice::flash_tx_led()
{
	HAL_GPIO_WritePin(m_p_device_hw_conf->LED_TX_GPIO_PORT,
	                  m_p_device_hw_conf->LED_TX_GPIO_PIN, GPIO_PIN_SET);
	m_ext_led_flashed = true;
	toggle_ext_led();
	reset_timer(m_p_device_hw_conf->p_tx_led_htim);
}

void LockoutDevice::on_rx_led_timer_overflow()
{
	HAL_GPIO_WritePin(m_p_device_hw_conf->LED_RX_GPIO_PORT,
	                  m_p_device_hw_conf->LED_RX_GPIO_PIN, GPIO_PIN_RESET);
}

void LockoutDevice::on_tx_led_timer_overflow()
{
	HAL_GPIO_WritePin(m_p_device_hw_conf->LED_TX_GPIO_PORT,
	                  m_p_device_hw_conf->LED_TX_GPIO_PIN, GPIO_PIN_RESET);
	if(m_ext_led_flashed)
	{
		m_ext_led_flashed = false;
		toggle_ext_led();
	}
}

void LockoutDevice::toggle_ext_led()
{
	if(HAL_GPIO_ReadPin(m_p_device_hw_conf->EXT_LED_GPIO_PORT, \
			m_p_device_hw_conf->EXT_LED_GPIO_PIN) == GPIO_PIN_RESET)
	{
		HAL_GPIO_WritePin(m_p_device_hw_conf->EXT_LED_GPIO_PORT,
		                  m_p_device_hw_conf->EXT_LED_GPIO_PIN, GPIO_PIN_SET);
	}
	else
	{
		HAL_GPIO_WritePin(m_p_device_hw_conf->EXT_LED_GPIO_PORT,
		                  m_p_device_hw_conf->EXT_LED_GPIO_PIN, GPIO_PIN_RESET);
	}
}

void LockoutDevice::on_radio_int()
{
	m_radio.on_radio_int();
}

void LockoutDevice::on_ping_button_press()
{
	m_ping_button = true;
}

void LockoutDevice::on_note_timer_overflow()
{
    m_buzzer.on_note_timer_overflow();
}

